<?php

namespace App\Http\Controllers;

use App\Models\CourseContent;
use App\Models\UnitContent;
use App\Models\Lesson;
use App\Models\Unit;
use App\Models\Course;
use App\Models\CourseUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
                'lesson_id' => 'required',
            ]);

            $user = Auth::user();
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $lesson_id = $request->lesson_id;

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $lesson_id)
            ->firstOrFail();

            $role = CourseUser::where('user_id', '=', $user->id)
            ->where('course_id', '=', $course_id)
            ->firstOrFail();

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $course['pivot'] = $role;

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $lesson = Lesson::where('id', $lesson_id)
            ->with('contents', 'contents.lesson_contentable')
            ->firstOrFail();

            return response()->json([
                'course' => $course,
                'unit' => $unit,
                'lesson' => $lesson,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    public function store(Request $request){
        try{
            $this->validate($request, [
                'unit_id'     => 'required',
                'name'     => 'required|string',
                'description'     => 'required|string',
            ]);

            $user = Auth::user();

            $count = UnitContent::where('unit_contentable_id', '=', $request->unit_id)
            ->count();

            $lesson = new Lesson([
                'name'    => $request->name,
                'description'    => $request->description,
                'cta_text'     => $request->cta_text ? $request->cta_text : null,
            ]);
            $lesson->save();

            $content = new UnitContent([
                'user_id'    => $user->id,
                'unit_id'    => $request->unit_id,
                'unit_contentable_id'    => $lesson->id,
                'unit_contentable_type'    => 'App\Models\Lesson',
                'order'     => $count,
            ]);
            $content->save();

            $content['unit_contentable'] = $lesson;

            return response()->json([
                'message' => 'The lesson was successfully created!',
                'content' => $content,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_id' => 'required',
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            $user = Auth::user();

            $lesson = Lesson::where('id', $request->lesson_id)
            ->firstOrFail();

            $lesson->name = $request->name;
            $lesson->description = $request->description;
            $lesson->cta_text = $request->cta_text ? $request->cta_text : null;
            $lesson->save();

            return response()->json([
                'message' => 'The lesson was successfully updated!',
                'lesson' => $lesson,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_id' => 'required',
            ]);

            $lesson = Lesson::where('id', $request->lesson_id)
            ->firstOrFail();
            $lesson->unit_content->delete();
            $lesson->delete();

            return response()->json([
                'message' => 'The lesson was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }
}
