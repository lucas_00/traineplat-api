<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\FormSubmit;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubmitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_user_form_submits(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'form_id' => 'required'
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $form_id = $request->form_id;

            $formSubmits = FormSubmit::where('user_id', $user->id)
            ->where('group_id', $group_id)
            ->where('form_id', $form_id)
            ->get();

            return response()->json([
                'userFormSubmits' => $formSubmits
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_group_form_submits(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'form_id' => 'required'
            ]);

            $group_id = $request->group_id;
            $form_id = $request->form_id;

            $formSubmits = User::with(["form_submits" => function($q) use($group_id, $form_id){
                $q->where('form_submits.group_id', '=', $group_id)
                ->where('form_submits.form_id', '=', $form_id);
            }])
            ->join('group_user', 'group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', $group_id)
            ->select(
                'users.id AS id',
                'users.name AS name',
                'users.email AS email',
                'group_user.user_id AS user_id',
                'group_user.group_id AS group_id',
                'group_user.role_user AS role_user'
            )
            ->get();

            return response()->json([
                'groupFormSubmits' => $formSubmits
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
