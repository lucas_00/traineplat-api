<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class LayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $groups = $user->groups()
            ->wherePivotIn('role_user', ['Owner', 'Admin'])
            ->get();
        $courses = $user->courses()
            ->wherePivotIn('role_user', ['Owner', 'Admin'])
            ->get();

        return response()->json([
            'groups' => $groups,
            'courses' => $courses
        ], 200);
    }
}
