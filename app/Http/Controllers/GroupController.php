<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Group;
use App\Models\Course;
use App\Models\CourseContent;
use App\Models\File;
use App\Models\Unit;
use App\Models\UnitContent;
use App\Models\Lesson;
use App\Models\Form;
use App\Models\FormSubmit;
use App\Models\GroupUser;
use App\Models\CourseGroup;
use App\Models\FormContentFillintheblank;
use App\Models\FormContentImage;
use App\Models\FormContentQuestion;
use App\Models\FormContentSelect;
use App\Models\FormContentMultiple;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Traits\FileUpload;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class GroupController extends Controller
{
    use FileUpload;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $groups = $user->groups()
            ->wherePivotIn('role_user', ['Owner', 'Admin'])
            ->get();

        return response()->json([
            'groups' => $groups,
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user = Auth::user();

        $groups = $user->groups()
            ->wherePivotIn('role_user', ['Teacher', 'Student'])
            ->with('courses')
            ->get();

        return response()->json([
            'groups' => $groups
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string',
            ]);
            $user = Auth::user();

            $group = new Group([
                'name'     => $request->name,
                'image_id'    => null,
                'user_id'    => $user->id,
            ]);
            $group->save();

            $image = null;

            if ($request->hasFile('files-0')) {
                $requestFile = $request->file('files-0');
                $file = $this->saveFiles($requestFile, 'images/');

                $image = new File([
                    'user_id'    => $user->id,
                    'fileable_id' => $group->id,
                    'fileable_type' => 'App\Models\Group',
                    'title'     => $file,
                    'original_name'     => $requestFile->getClientOriginalName(),
                    'src'     => ('/uploads/images/' . $file),
                ]);

                $image->save();
            }

            $user->groups()
            ->attach(
                $group->id,
                [
                    'role_user' => 'Owner'
                ],
            );

            $group = $user->groups()
                ->wherePivot('group_id', '=', $group->id)
                ->first();

            return response()->json([
                'message' => 'The Group was successfully created!',
                'group' => $group,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try {
            $user = Auth::user();
            $group_id = $request->id;

            $role = GroupUser::where('group_id', '=', $group_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $group = Group::with('notification')
            ->with('courses')
            ->with(["comments" => function($q) use($group_id){
                $q->where('comments.commentable_id', $group_id);
                $q->where('comments.course_id', null);
                $q->where('comments.message_id', null);
                $q->where('comments.unit_id', null);
                $q->where('comments.lesson_id', null);
            }])
            ->with('user')
            ->where('id', $group_id)
            ->firstOrFail();

            $group['role'] = $role;

            return response()->json([
                'group' => $group,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string'
            ]);
            $user = Auth::user();

            $group = $user->groups()
                ->wherePivot('group_id', '=', $request->id)
                ->first();

            $image = null;

            if ($request->hasFile('files-0')) {
                $requestFile = $request->file('files-0');
                $file = $this->saveFiles($requestFile, 'images/');

                $image = new File([
                    'user_id'    => $user->id,
                    'fileable_id' => $group->id,
                    'fileable_type' => 'App\Models\Group',
                    'title'     => $file,
                    'original_name'     => $requestFile->getClientOriginalName(),
                    'src'     => ('/uploads/images/' . $file),
                ]);

                if ($group->logo != null) {
                    $group->logo->delete();
                }

                $image->save();
            }

            $group->name = $request->name;
            $group->save();
            $group->refresh();

            return response()->json([
                'message' => 'The Group was successfully updated!',
                'group' => $group,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
            ]);
            $user = Auth::user();

            $group = $user->groups()
                ->wherePivot('group_id', '=', $request->group_id)
                ->first();

            $group->users()
                ->detach();

            $group->delete();

            return response()->json([
                'message' => 'The Group was successfully removed!',
                'group' => $group,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscriptions(Request $request)
    {
        try {
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $courses = $group->courses;

            return response()->json([
                'courses' => $courses,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function unit(Request $request)
    {
        try {
            $this->validate($request, [
                'group_id' => 'required',
                'course_id' => 'required',
                'unit_id' => 'required',
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;

            $courseGroup = CourseGroup::select('id')
            ->where('group_id', $group_id)
            ->where('course_id', $course_id)
            ->firstOrFail();

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $group = Group::with(["comments" => function($q) use($group_id, $unit_id){
                $q->where('comments.commentable_id', $group_id);
                $q->where('comments.course_id', null);
                $q->where('comments.message_id', null);
                $q->where('comments.unit_id', $unit_id);
                $q->where('comments.lesson_id', null);
            }])
            ->where('id', $group_id)
            ->firstOrFail();

            $group['pivot'] = $role;

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $unit = Unit::where('id', $unit_id)
            ->with(['contents', 'contents.unit_contentable' => function (MorphTo $morphTo) use($user) {
                $morphTo->morphWithCount([
                    Form::class => ['form_submits' => function ($q) use($user){
                        $q->where('form_submits.user_id', $user->id);
                    }]
                ]);
            }])
            ->firstOrFail();

            return response()->json([
                'course' => $course,
                'group' => $group,
                'unit' => $unit,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lesson(Request $request)
    {
        try {
            $this->validate($request, [
                'group_id' => 'required',
                'course_id' => 'required',
                'unit_id' => 'required',
                'lesson_id' => 'required',
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $lesson_id = $request->lesson_id;

            $courseGroup = CourseGroup::select('id')
            ->where('group_id', $group_id)
            ->where('course_id', $course_id)
            ->firstOrFail();

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $lesson_id)
            ->firstOrFail();

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $group = Group::with(["comments" => function($q) use($group_id, $lesson_id){
                $q->where('comments.commentable_id', $group_id);
                $q->where('comments.course_id', null);
                $q->where('comments.message_id', null);
                $q->where('comments.unit_id', null);
                $q->where('comments.lesson_id', $lesson_id);
            }])
            ->where('id', $group_id)
            ->firstOrFail();

            $group['pivot'] = $role;

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $lesson = Lesson::where('id', $lesson_id)
            ->with('contents', 'contents.lesson_contentable')
            ->firstOrFail();

            return response()->json([
                'group' => $group,
                'course' => $course,
                'unit' => $unit,
                'lesson' => $lesson,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    /**
     * Show
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function form(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;

            $courseGroup = CourseGroup::select('id')
            ->where('group_id', $group_id)
            ->where('course_id', $course_id)
            ->firstOrFail();

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $group = Group::select('id', 'name')
            ->where('id', $group_id)
            ->firstOrFail();

            $group['pivot'] = $role;

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::query()
            ->where('id', $form_id)
            ->with(['contents.form_contentable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    FormContentImage::class => ['contents'],
                    FormContentMultiple::class => ['question.options'],
                    FormContentQuestion::class => ['question.options'],
                    FormContentSelect::class => ['question.options'],
                    FormContentFillintheblank::class => ['contents.fillintheblank_contentable.question.options'],
                ]);
            }])
            ->firstOrFail();

            $formSubmits = FormSubmit::where('user_id', $user->id)
            ->where('group_id', $group_id)
            ->where('form_id', $form_id)
            ->count();

            $form['formSubmits'] = $formSubmits;

            return response()->json([
                'group' => $group,
                'course' => $course,
                'unit' => $unit,
                'form' => $form
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show_submit(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
                'submit_id' => 'required',
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;
            $submit_id = $request->submit_id;

            $courseGroup = CourseGroup::select('id')
            ->where('group_id', $group_id)
            ->where('course_id', $course_id)
            ->firstOrFail();

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $group = Group::select('id', 'name')
            ->where('id', $group_id)
            ->firstOrFail();

            $group['pivot'] = $role;

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::query()
            ->where('id', $form_id)
            ->with(['contents.form_contentable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    FormContentImage::class => ['contents'],
                    FormContentMultiple::class => ['question.options.correct'],
                    FormContentQuestion::class => ['question.options.correct'],
                    FormContentSelect::class => ['question.options.correct'],
                    FormContentFillintheblank::class => ['contents.fillintheblank_contentable.question.options.correct'],
                ]);
            }])
            ->firstOrFail();

            $formSubmit = FormSubmit::where('id', $submit_id)
            ->where('form_id', $form_id)
            ->with('user', 'answers')
            ->firstOrFail();

            if ($role->role_user != "Student" || $formSubmit->user_id == $user->id) {
                return response()->json([
                    'group' => $group,
                    'course' => $course,
                    'unit' => $unit,
                    'form' => $form,
                    'formSubmit' => $formSubmit
                ], 201);
            }else{
                abort(404);
            }

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function course(Request $request)
    {
        try {
            $this->validate($request, [
                'course_id' => 'required',
                'group_id' => 'required'
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;

            $exist = CourseGroup::where('course_id', '=', $course_id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $group = Group::with(["comments" => function($q) use($group_id, $course_id){
                $q->where('comments.commentable_id', $group_id);
                $q->where('comments.course_id', $course_id);
                $q->where('comments.message_id', null);
                $q->where('comments.unit_id', null);
                $q->where('comments.lesson_id', null);
            }])
            ->where('id', $group_id)
            ->firstOrFail();

            $group['pivot'] = $role;

            $course = Course::where('id', '=', $course_id)
            ->with('contents', 'contents.course_contentable')
            ->firstOrFail();

            return response()->json([
                'course' => $course,
                'group' => $group,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_course(Request $request)
    {
        try {
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $group->courses()
            ->attach(
                $course->id,
            );

            return response()->json([
                'message' => 'The course was successfully added!',
                'course' => $course,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_courses(Request $request)
    {
        $user = Auth::user();

        $courses = $user->courses()
        ->wherePivot('role_user', '=', 'Owner')
        ->orWherePivot('role_user', '=', 'Admin')
        ->get();

        return response()->json([
            'courses' => $courses,
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_course(Request $request)
    {
        try {
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $group->courses()
            ->detach(
                $course->id,
            );

            return response()->json([
                'message' => 'The course was successfully removed!',
                'course' => $course
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function get_users(Request $request){
        try {
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $users = $group->users;

            return response()->json([
                'users' => $users
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function add_user(Request $request){
        try {
            $this->validate($request, [
                'email'    => 'required|string|email',
            ]);
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $newUser = User::where('email', $request->email)->first();

            if (!$newUser) {
                $newUser['email'] = $request->email;
                $newUser['name'] = $request->email;
                $newUser['password'] = Hash::make($request->email);
                $newUser = User::create($newUser);
            }

            if (!$group->users->contains('id', $newUser->id)) {
                $newUser->groups()
                ->attach(
                    $group->id,
                    [
                        'role_user' => $request->role,
                    ]
                );

                $newUser = $group->users()
                ->wherePivot('user_id', '=', $newUser->id)
                ->first();
            }else{
                return response([
                    'message' => 'The user is already a member of this group.'
                ], 403);
            }

            return response()->json([
                'message' => 'The user was successfully added!',
                'user' => $newUser
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_user(Request $request)
    {
        try {
            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $member = $group->users()
            ->wherePivot('user_id', '=', $request->user_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Admin')
                    ->orWhere('role_user', '=', 'Teacher')
                    ->orWhere('role_user', '=', 'Student');
            })
            ->firstOrFail();

            $group->users()
            ->detach(
                $member->id,
            );

            return response()->json([
                'message' => 'The member was successfully removed!',
                'user' => $member
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_role_user(Request $request)
    {
        try {
            $this->validate($request, [
                'group_id' => 'required',
                'user_id' => 'required',
                'role_user' => 'required',
            ]);

            $user = Auth::user();

            $group = $user->groups()
            ->wherePivot('group_id', '=', $request->group_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $member = GroupUser::where('user_id', $request->user_id)
            ->where('group_id', $request->group_id)
            ->firstOrFail();

            $member->role_user = $request->role_user;
            $member->save();

            return response()->json([
                'message' => 'The member was successfully updated!',
                'user' => $member
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
