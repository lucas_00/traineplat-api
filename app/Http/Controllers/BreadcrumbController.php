<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\GroupUser;
use App\Models\Group;
use App\Models\CourseGroup;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BreadcrumbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_group(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required'
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $breadcrumb = Group::where('id', $group_id)
            ->select(
                'groups.name AS group_name',
                'groups.id AS group_id'
            )
            ->firstOrFail();

            return response()->json([
                'breadcrumb' => $breadcrumb,
                'role' => $role
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_group_course_unit_form(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required'
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();

            $breadcrumb = CourseGroup::where('course_group.group_id', $group_id)
            ->where('course_group.course_id', $course_id)
            ->where('units.id', $unit_id)
            ->where('forms.id', $form_id)
            ->join('groups', 'groups.id', '=', 'course_group.group_id')
            ->join('courses', 'courses.id', '=', 'course_group.course_id')
            ->join('course_contents', 'course_contents.course_id', '=', 'courses.id')
            ->join('units', 'units.id', '=', 'course_contents.course_contentable_id')
            ->join('unit_contents', 'unit_contents.unit_id', '=', 'units.id')
            ->join('forms', 'forms.id', '=', 'unit_contents.unit_contentable_id')
            ->select(
                'groups.name AS group_name',
                'groups.id AS group_id',
                'courses.name AS course_name',
                'courses.id AS course_id',
                'forms.name AS form_name',
                'forms.id AS form_id',
                'units.name AS unit_name',
                'units.id AS unit_id'
            )
            ->firstOrFail();

            return response()->json([
                'breadcrumb' => $breadcrumb,
                'role' => $role
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
