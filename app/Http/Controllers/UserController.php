<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function update_name(Request $request)
    {
        try{
            $this->validate($request, [
                'name'     => 'required|string',
            ]);

            $user = Auth::user();
            $user->name = $request->name;
            $user->save();

            return response()->json([
                'message' => 'The Name was successfully updated!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    public function update_email(Request $request)
    {
        try{
            $this->validate($request, [
                'email'    => 'required|string|email|unique:users',
            ]);

            $user = Auth::user();
            $user->email = $request->email;
            $user->save();

            return response()->json([
                'message' => 'Email was successfully updated!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            if ($e->status === 422) {
                return response([
                    'message' => 'The email has already been taken'
                ], $e->status);
            }

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    public function update_password(Request $request)
    {
        try{
            $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
            ]);

            $user = Auth::user();
            $user->password = Hash::make($request->password);;
            $user->save();

            return response()->json([
                'message' => 'Password was successfully updated!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
