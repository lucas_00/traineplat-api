<?php

namespace App\Http\Controllers;

use App\Models\CourseUser;
use App\Models\CourseContent;
use App\Models\Unit;
use App\Models\UnitContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
            ]);

            $user = Auth::user();

            $courseContent = CourseContent::where('course_id', $request->course_id)
            ->where('course_contentable_id', $request->unit_id)
            ->firstOrFail();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->firstOrFail();

            $unit = Unit::where('id', $request->unit_id)
            ->with('contents', 'contents.unit_contentable')
            ->firstOrFail();

            return response()->json([
                'unit' => $unit,
                'course' => $course,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    public function store(Request $request){
        try{
            $this->validate($request, [
                'course_id'     => 'required',
                'name'     => 'required|string',
                'description'     => 'required|string',
            ]);

            $user = Auth::user();

            $role = CourseUser::where('course_id', '=', $request->course_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $count = CourseContent::where('course_id', '=', $request->course_id)
            ->count();

            $unit = new Unit([
                'name'    => $request->name,
                'description'    => $request->description,
                'cta_text'     => $request->cta_text ? $request->cta_text : null,
            ]);
            $unit->save();

            $content = new CourseContent([
                'user_id'    => $user->id,
                'course_id'    => $request->course_id,
                'course_contentable_id'    => $unit->id,
                'course_contentable_type'    => 'App\Models\Unit',
                'order'     => $count,
                'image_id'     => null,
            ]);
            $content->save();

            $content['course_contentable'] = $unit;

            return response()->json([
                'message' => 'The content was successfully created!',
                'content' => $content,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'unit_id' => 'required',
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            $user = Auth::user();

            $content = CourseContent::where('course_contentable_id', '=', $request->unit_id)
            ->with('course_contentable')
            ->firstOrFail();

            $role = CourseUser::where('course_id', '=', $content->course_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $content->course_contentable->name = $request->name;
            $content->course_contentable->description = $request->description;
            $content->course_contentable->cta_text = $request->cta_text ? $request->cta_text : null;
            $content->course_contentable->save();

            return response()->json([
                'message' => 'The Unit was successfully updated!',
                'unit' => $content->course_contentable,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'unit_id' => 'required',
            ]);
            $user = Auth::user();

            $content = CourseContent::where('course_contentable_id', '=', $request->unit_id)
            ->with('course_contentable')
            ->firstOrFail();

            $role = CourseUser::where('course_id', '=', $content->course_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $content->course_contentable->delete();
            $content->delete();

            return response()->json([
                'message' => 'The Content was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_forms(Request $request)
    {
        $user = Auth::user();

        $forms = $user->forms()
        ->wherePivot('role_user', '=', 'Owner')
        ->orWherePivot('role_user', '=', 'Admin')
        ->get();

        return response()->json([
            'forms' => $forms,
        ], 200);
    }
}
