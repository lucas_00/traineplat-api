<?php

namespace App\Http\Controllers;

use App\Models\LessonContent;
use App\Models\LessonContentText;
use App\Models\LessonContentVideo;
use App\Models\LessonContentLink;
use App\Models\LessonContentImage;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Traits\FileUpload;

class LessonContentController extends Controller
{
    use FileUpload;

    public function store_text(Request $request){
        try{
            $this->validate($request, [
                'lesson_id'     => 'required',
                'content'     => 'required|string',
            ]);

            $user = Auth::user();

            $count = LessonContent::where('lesson_id', '=', $request->lesson_id)
            ->count();

            $lessonContentText = new LessonContentText([
                'content'    => $request->content,
            ]);
            $lessonContentText->save();

            $lessonContent = new lessonContent([
                'user_id'    => $user->id,
                'lesson_id'    => $request->lesson_id,
                'lesson_contentable_id'    => $lessonContentText->id,
                'lesson_contentable_type'    => 'App\Models\LessonContentText',
                'order'     => $count,
            ]);
            $lessonContent->save();

            $lessonContent['lesson_contentable'] = $lessonContentText;

            return response()->json([
                'message' => 'The content was successfully added!',
                'content' => $lessonContent,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function store_video(Request $request){
        try{
            $this->validate($request, [
                'lesson_id'     => 'required',
                'content'     => 'required|string',
            ]);

            $user = Auth::user();

            $count = LessonContent::where('lesson_id', '=', $request->lesson_id)
            ->count();

            $lessonContentVideo = new LessonContentVideo([
                'content'    => $request->content,
            ]);
            $lessonContentVideo->save();

            $lessonContent = new LessonContent([
                'user_id'    => $user->id,
                'lesson_id'    => $request->lesson_id,
                'lesson_contentable_id'    => $lessonContentVideo->id,
                'lesson_contentable_type'    => 'App\Models\LessonContentVideo',
                'order'     => $count,
            ]);
            $lessonContent->save();

            $lessonContent['lesson_contentable'] = $lessonContentVideo;

            return response()->json([
                'message' => 'The content was successfully added!',
                'content' => $lessonContent,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function store_link(Request $request){
        try{
            $this->validate($request, [
                'lesson_id'     => 'required',
                'content'     => 'required|string',
            ]);

            $user = Auth::user();

            $count = LessonContent::where('lesson_id', '=', $request->lesson_id)
            ->count();

            $lessonContentLink = new LessonContentLink([
                'content'    => $request->content,
            ]);
            $lessonContentLink->save();

            $lessonContent = new LessonContent([
                'user_id'    => $user->id,
                'lesson_id'    => $request->lesson_id,
                'lesson_contentable_id'    => $lessonContentLink->id,
                'lesson_contentable_type'    => 'App\Models\LessonContentLink',
                'order'     => $count,
            ]);
            $lessonContent->save();

            $lessonContent['lesson_contentable'] = $lessonContentLink;

            return response()->json([
                'message' => 'The content was successfully added!',
                'content' => $lessonContent,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function store_image(Request $request){
        try{
            $this->validate($request, [
                'lesson_id'     => 'required',
            ]);

            $user = Auth::user();

            $count = LessonContent::where('lesson_id', '=', $request->lesson_id)
            ->count();

            $lessonContentImage = new LessonContentImage();
            $lessonContentImage->save();

            $images = array();

            for ($i = 0; $i < count($request->files); $i++) {
                if ($request->hasFile('files-' . $i)) {
                    $requestFile = $request->file('files-' . $i);
                    $file = $this->saveFiles($requestFile, 'images/');

                    $image = new File([
                        'user_id'    => $user->id,
                        'fileable_id' => $lessonContentImage->id,
                        'fileable_type' => 'App\Models\LessonContentImage',
                        'title'     => $file,
                        'original_name'     => $requestFile->getClientOriginalName(),
                        'src'     => ('/uploads/images/' . $file),
                    ]);

                    $image->save();

                    array_push($images, $image);
                }
            }

            $lessonContentImage->contents = $images;

            $lessonContent = new LessonContent([
                'user_id'    => $user->id,
                'lesson_id'    => $request->lesson_id,
                'lesson_contentable_id'    => $lessonContentImage->id,
                'lesson_contentable_type'    => 'App\Models\LessonContentImage',
                'order'     => $count,
            ]);
            $lessonContent->save();

            $lessonContent['lesson_contentable'] = $lessonContentImage;

            return response()->json([
                'message' => 'The content was successfully added!',
                'content' => $lessonContent,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update_text(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $lessonContent = LessonContent::where('id', $request->lesson_content_id)
            ->firstOrFail();

            $lessonContent->lesson_contentable->content = $request->content;
            $lessonContent->lesson_contentable->save();

            return response()->json([
                'message' => 'The content was successfully updated!',
                'content' => $lessonContent->lesson_contentable->content,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update_video(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $lessonContent = LessonContent::where('id', $request->lesson_content_id)
            ->firstOrFail();

            $lessonContent->lesson_contentable->content = $request->content;
            $lessonContent->lesson_contentable->save();

            return response()->json([
                'message' => 'The content was successfully updated!',
                'content' => $lessonContent->lesson_contentable->content,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update_link(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $lessonContent = LessonContent::where('id', $request->lesson_content_id)
            ->firstOrFail();

            $lessonContent->lesson_contentable->content = $request->content;
            $lessonContent->lesson_contentable->save();

            return response()->json([
                'message' => 'The content was successfully updated!',
                'content' => $lessonContent->lesson_contentable->content,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\File  $image
     * @return \Illuminate\Http\Response
     */
    public function update_image(Request $request)
    {
        try{
            $this->validate($request, [
                'image_id' => 'required',
            ]);

            $image = File::where('id', $request->image_id)
            ->firstOrFail();

            if (!$request->hasFile('files-0')) {

                return response([
                    'message' => 'Something went wrong on the server'
                ], 404);

            }else{
                $requestFile = $request->file('files-0');
                $file = $this->saveFiles($requestFile, 'images/');

                $image->title = $file;
                $image->original_name = $requestFile->getClientOriginalName();
                $image->src = ('/uploads/images/' . $file);

                $image->save();
            }

            return response()->json([
                'message' => 'The image was successfully updated!',
                'image' => $image,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy_image(Request $request)
    {
        try{
            $this->validate($request, [
                'image_id' => 'required',
            ]);

            $image = File::where('id', $request->image_id)
            ->firstOrFail();
            $image->delete();

            return response()->json([
                'message' => 'The image was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function append_image(Request $request){
        try{
            $this->validate($request, [
                'lesson_content_image_id'     => 'required',
            ]);

            $user = Auth::user();

            $lessonContentImage = LessonContentImage::select('id')
            ->where('id', '=', $request->lesson_content_image_id)
            ->firstOrFail();

            $images = array();

            for ($i = 0; $i < count($request->files); $i++) {
                if ($request->hasFile('files-' . $i)) {
                    $requestFile = $request->file('files-' . $i);
                    $file = $this->saveFiles($requestFile, 'images/');

                    $image = new File([
                        'user_id'    => $user->id,
                        'fileable_id' => $lessonContentImage->id,
                        'fileable_type' => 'App\Models\LessonContentImage',
                        'title'     => $file,
                        'original_name'     => $requestFile->getClientOriginalName(),
                        'src'     => ('/uploads/images/' . $file),
                    ]);

                    $image->save();

                    array_push($images, $image);
                }
            }

            return response()->json([
                'message' => 'The content was successfully added!',
                'content' => $images,
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'lesson_content_id' => 'required',
            ]);

            $lessonContent = LessonContent::where('id', $request->lesson_content_id)
            ->firstOrFail();
            $lessonContent->lesson_contentable->delete();
            $lessonContent->delete();

            return response()->json([
                'message' => 'The content was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }
}
