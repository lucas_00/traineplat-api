<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try{
            $this->validate($request, [
                'email'    => 'required|email',
                'password' => 'required|string',
            ]);

            $user = User::where('email', $request->email)->first();

            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'message' => 'These credentials do not match our records.'
                ], 404);
            }

            $token = $user->createToken('my-app-token')->plainTextToken;

            $response = [
                'user' => $user,
                'token' => $token,
            ];

            return response($response, 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            if ($e->status === 422) {
                return response([
                    'message' => 'The email has already been taken'
                ], $e->status);
            }

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function signup(Request $request)
    {
        try{
            $this->validate($request, [
                'name'     => 'required|string',
                'email'    => 'required|string|email|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            $user = $request->all();
            $user['password'] = Hash::make($user['password']);

            $user = User::create($user);

            return response()->json([
                'message' => 'User successfully created!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            if ($e->status === 422) {
                return response([
                    'message' => 'The email has already been taken'
                ], $e->status);
            }

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    public function logout()
    {
        Auth::logout();
        return response()->json(['message' => 'Logged Out'], 200);
    }

    public function forgot_password(Request $request)
    {
        $request->validate(['email' => 'required|email']);
        $status = Password::sendResetLink(
            $request->only('email')
        );
        return $status === Password::RESET_LINK_SENT
            ? response()->json(['message' => [__($status)]], 200)
            : response()->json(['message' => [__($status)]], 400);
    }

    public function reset_password_update(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
                $user->save();
                event(new PasswordReset($user));
            }
        );
        return $status === Password::PASSWORD_RESET
            ? response()->json(['message' => [__($status)]], 200)
            : response()->json(['message' => [__($status)]], 400);
    }
}
