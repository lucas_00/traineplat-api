<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\GroupUser;
use App\Models\FormSubmit;
use App\Models\Form;
use App\Models\Lesson;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class UserProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => [
                    'required',
                    'numeric'
                ]
            ]);

            $user = Auth::user();
            $group_id = $request->group_id;

            $role = GroupUser::where('user_id', '=', $user->id)
            ->where('group_id', '=', $group_id)
            ->firstOrFail();
            $group = Group::select('id', 'name')
            ->where('id', $group_id)
            ->with([
                'users:id,name,email',
                'courses:id,name',
                'courses.contents:id,user_id,course_id,course_contentable_id,course_contentable_type,order',
                'courses.contents.course_contentable:id,name',
                'courses.contents.course_contentable.contents:id,user_id,unit_id,unit_contentable_id,unit_contentable_type,order',
                'courses.contents.course_contentable.contents.unit_contentable' => function (MorphTo $morphTo) {
                    $morphTo->morphWith([
                        Form::class => ['form_submits:id,group_id,user_id,form_id,is_checked,created_at'],
                        Lesson::class => []
                    ])
                    ->select('id', 'name');
                }
            ])
            ->firstOrFail();

            return response()->json([
                'group' => $group
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], $e->status);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_user_form_submits(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'user_id' => 'required',
                'form_id' => 'required'
            ]);

            $group_id = $request->group_id;
            $user_id = $request->user_id;
            $form_id = $request->form_id;

            $formSubmits = FormSubmit::where('user_id', $user_id)
            ->where('group_id', $group_id)
            ->where('form_id', $form_id)
            ->get();

            return response()->json([
                'userFormSubmits' => $formSubmits
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
