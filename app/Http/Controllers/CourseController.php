<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\User;
use App\Models\File;
use App\Models\CourseUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Traits\FileUpload;

class CourseController extends Controller
{
    use FileUpload;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $courses = $user->courses()
            ->wherePivotIn('role_user', ['Owner', 'Admin'])
            ->get();

        return response()->json([
            'courses' => $courses,
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user = Auth::user();

        $ownerCourses = $user->courses()
            ->wherePivot('role_user', '=', 'Owner')
            ->get();

        $adminCourses = $user->courses()
            ->wherePivot('role_user', '=', 'Admin')
            ->get();

        return response()->json([
            'owner_courses' => $ownerCourses,
            'admin_courses' => $adminCourses,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            $user = Auth::user();

            $course = new Course([
                'name'     => $request->name,
                'description'     => $request->description,
                'cta_text'     => $request->cta_text ? $request->cta_text : null,
                'image_id'    => null,
                'user_id'    => $user->id,
            ]);
            $course->save();

            $image = null;

            if ($request->hasFile('files-0')) {
                $requestFile = $request->file('files-0');
                $file = $this->saveFiles($requestFile, 'images/');

                $image = new File([
                    'user_id'    => $user->id,
                    'fileable_id' => $course->id,
                    'fileable_type' => 'App\Models\Course',
                    'title'     => $file,
                    'original_name'     => $requestFile->getClientOriginalName(),
                    'src'     => ('/uploads/images/' . $file),
                ]);

                $image->save();
            }

            $user->courses()
            ->attach(
                $course->id,
                [
                    'role_user' => 'Owner'
                ]
            );

            $course = $user->courses()
                ->wherePivot('course_id', '=', $course->id)
                ->first();

            return response()->json([
                'message' => 'The Course was successfully created!',
                'course' => $course,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $this->validate($request, [
                'id' => 'required',
            ]);

            $user = Auth::user();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->id)
            ->with('contents', 'contents.course_contentable')
            ->firstOrFail();

            return response()->json([
                'course' => $course,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            $user = Auth::user();

            $course = $user->courses()
                ->wherePivot('course_id', '=', $request->id)
                ->first();

            $image = null;

            if ($request->hasFile('files-0')) {
                $requestFile = $request->file('files-0');
                $file = $this->saveFiles($requestFile, 'images/');

                $image = new File([
                    'user_id'    => $user->id,
                    'fileable_id' => $course->id,
                    'fileable_type' => 'App\Models\Course',
                    'title'     => $file,
                    'original_name'     => $requestFile->getClientOriginalName(),
                    'src'     => ('/uploads/images/' . $file),
                ]);

                if ($course->logo != null) {
                    $course->logo->delete();
                }

                $image->save();
            }

            $course->name = $request->name;
            $course->description = $request->description;
            $course->cta_text = $request->cta_text ? $request->cta_text : null;
            $course->save();
            $course->refresh();

            return response()->json([
                'message' => 'The Course was successfully updated!',
                'course' => $course,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
            ]);
            $user = Auth::user();

            $course = $user->courses()
                ->wherePivot('course_id', '=', $request->course_id)
                ->first();

            $course->users()
                ->detach();

            $course->groups()
                ->detach();

            $course->delete();

            return response()->json([
                'message' => 'The Course was successfully removed!',
                'course' => $course,
            ], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    public function get_users(Request $request){
        try {
            $user = Auth::user();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $users = $course->users;

            return response()->json([
                'users' => $users
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function add_user(Request $request){
        try {
            $this->validate($request, [
                'email'    => 'required|string|email',
            ]);
            $user = Auth::user();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $newUser = User::where('email', $request->email)->first();

            if (!$newUser) {
                $newUser['email'] = $request->email;
                $newUser['name'] = $request->email;
                $newUser['password'] = Hash::make($request->email);
                $newUser = User::create($newUser);
            }

            if (!$course->users->contains('id', $newUser->id)) {
                $newUser->courses()
                ->attach(
                    $course->id,
                    [
                        'role_user' => $request->role,
                    ]
                );

                $newUser = $course->users()
                ->wherePivot('user_id', '=', $newUser->id)
                ->first();
            }else{
                return response([
                    'message' => 'The user is already a member of this course.'
                ], 403);
            }

            return response()->json([
                'message' => 'The user was successfully added!',
                'user' => $newUser
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_user(Request $request)
    {
        try {
            $user = Auth::user();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $member = $course->users()
            ->wherePivot('user_id', '=', $request->user_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Admin')
                    ->orWhere('role_user', '=', 'Teacher')
                    ->orWhere('role_user', '=', 'Student');
            })
            ->firstOrFail();

            $course->users()
            ->detach(
                $member->id,
            );

            return response()->json([
                'message' => 'The member was successfully removed!',
                'user' => $member
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_role_user(Request $request)
    {
        try {
            $this->validate($request, [
                'course_id' => 'required',
                'user_id' => 'required',
                'role_user' => 'required',
            ]);

            $user = Auth::user();

            $course = $user->courses()
            ->wherePivot('course_id', '=', $request->course_id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $member = CourseUser::where('user_id', $request->user_id)
            ->where('course_id', $request->course_id)
            ->firstOrFail();

            $member->role_user = $request->role_user;
            $member->save();

            return response()->json([
                'message' => 'The member was successfully updated!',
                'user' => $member
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
