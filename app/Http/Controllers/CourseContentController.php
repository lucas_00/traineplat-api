<?php

namespace App\Http\Controllers;

use App\Models\CourseUser;
use App\Models\CourseContent;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CourseContentController extends Controller
{

    public function index(Request $request){
        try{
            $user = Auth::user();

            $role = CourseUser::where('course_id', '=', $request->course_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $contents = CourseContent::where('course_id', '=', $request->course_id)
            ->with('course_contentable')
            ->get();

            return response()->json([
                'contents' => $contents,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }
}
