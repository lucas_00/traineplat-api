<?php

namespace App\Http\Controllers;

use App\Models\CourseContent;
use App\Models\UnitContent;
use App\Models\Group;
use App\Models\Form;
use App\Models\FormSubmit;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Unit;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\FormContentFillintheblank;
use App\Models\FormContentImage;
use App\Models\FormContentQuestion;
use App\Models\FormContentSelect;
use App\Models\FormContentMultiple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphTo;


class FormController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
            ]);

            $user = Auth::user();
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = CourseUser::where('user_id', '=', $user->id)
            ->where('course_id', '=', $course_id)
            ->firstOrFail();

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $course['pivot'] = $role;

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::query()
            ->where('id', $form_id)
            ->with(['contents.form_contentable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    FormContentImage::class => ['contents'],
                    FormContentMultiple::class => ['question.options.correct'],
                    FormContentQuestion::class => ['question.options.correct'],
                    FormContentSelect::class => ['question.options.correct'],
                    FormContentFillintheblank::class => ['contents.fillintheblank_contentable.question.options.correct'],
                ]);
            }])
            ->firstOrFail();

            return response()->json([
                'course' => $course,
                'unit' => $unit,
                'form' => $form,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statistics(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
            ]);

            $user = Auth::user();
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = CourseUser::where('user_id', '=', $user->id)
            ->where('course_id', '=', $course_id)
            ->firstOrFail();

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->with('groups:id,name', 'groups.users')
            ->firstOrFail();

            $course['pivot'] = $role;

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::where('id', $form_id)
            ->with(['contents.form_contentable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    FormContentMultiple::class => ['question.options', 'question.answers'],
                    FormContentQuestion::class => ['question.options', 'question.answers'],
                    FormContentSelect::class => ['question.options', 'question.answers'],
                    FormContentFillintheblank::class => ['contents.fillintheblank_contentable.question.options', 'contents.fillintheblank_contentable.question.answers'],
                ]);
            }])
            ->firstOrFail();

            // get GroupFormSubmits
            $groups = $course->groups;

            // get stats
            for ($i=0; $i < count($groups); $i++) {
                $group = $groups[$i];
                $aux_form_submits = FormSubmit::where([['form_id', '=', $form->id], ['group_id', '=', $group->id]])
                ->select('id', 'created_at', 'form_id', 'group_id', 'is_checked', 'user_id')
                ->has('group')
                ->with('answers:id,content,form_submit_id,group_id,question_id,user_id')
                ->get();
                $group['stats'] = $this->getAnwersData($aux_form_submits);
            }

            // get AllFormSubmits
            $allFormSubmits = [
                'form_submits' => (array)[],
                'stats' => (object)[]
            ];

            $allFormSubmits['form_submits'] = FormSubmit::where('form_id', '=', $form->id)
            ->select('id', 'created_at', 'form_id', 'group_id', 'is_checked', 'user_id')
            ->has('group')
            ->with('answers:id,content,form_submit_id,group_id,question_id,user_id')
            ->get();
            // get stats
            $allFormSubmits['stats'] = $this->getAnwersData($allFormSubmits['form_submits']);

            return response()->json([
                'course' => $course,
                'unit' => $unit,
                'form' => $form,
                'groups' => $groups,
                'allFormSubmits' => $allFormSubmits,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit_answers(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
            ]);

            $user = Auth::user();
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = CourseUser::where('user_id', '=', $user->id)
            ->where('course_id', '=', $course_id)
            ->firstOrFail();

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->with('groups:id,name', 'groups.users')
            ->firstOrFail();

            $course['pivot'] = $role;

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::select('id', 'name')
            ->where('id', $form_id)
            ->firstOrFail();

            // get GroupFormSubmits
            $groups = $course->groups;

            $formSubmits = FormSubmit::where('form_id', $form->id)
            ->get();

            for ($i=0; $i < count($groups); $i++) {
                $group = $groups[$i];
                for ($y=0; $y < count($group->users); $y++) {
                    $user = $group->users[$y];
                    $auxReturn = (array)[];
                    for ($x=0; $x < count($formSubmits); $x++) {
                        $formSubmit = $formSubmits[$x];
                        if ($formSubmit->user_id == $user->id && $formSubmit->group_id == $group->id) {
                            $formSubmit['date'] = $formSubmit->created_at->toDateTimeString();
                            array_push($auxReturn, $formSubmit);
                        }
                    }

                    $user['submits'] = $auxReturn;
                }
            }

            return response()->json([
                'course' => $course,
                'unit' => $unit,
                'form' => $form,
                'groups' => $groups
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show_submit(Request $request)
    {
        try{
            $this->validate($request, [
                'course_id' => 'required',
                'unit_id' => 'required',
                'form_id' => 'required',
                'submit_id' => 'required',
            ]);

            $user = Auth::user();
            $course_id = $request->course_id;
            $unit_id = $request->unit_id;
            $form_id = $request->form_id;
            $submit_id = $request->submit_id;

            $courseContent = CourseContent::select('id')
            ->where('course_id', $course_id)
            ->where('course_contentable_id', $unit_id)
            ->firstOrFail();

            $unitContent = UnitContent::select('id')
            ->where('unit_id', $unit_id)
            ->where('unit_contentable_id', $form_id)
            ->firstOrFail();

            $role = CourseUser::where('user_id', '=', $user->id)
            ->where('course_id', '=', $course_id)
            ->firstOrFail();

            $course = Course::select('id', 'name')
            ->where('id', $course_id)
            ->firstOrFail();

            $course['pivot'] = $role;

            $unit = Unit::select('id', 'name')
            ->where('id', $unit_id)
            ->firstOrFail();

            $form = Form::query()
            ->where('id', $form_id)
            ->with(['contents.form_contentable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    FormContentImage::class => ['contents'],
                    FormContentMultiple::class => ['question.options.correct'],
                    FormContentQuestion::class => ['question.options.correct'],
                    FormContentSelect::class => ['question.options.correct'],
                    FormContentFillintheblank::class => ['contents.fillintheblank_contentable.question.options.correct']
                ]);
            }])
            ->firstOrFail();

            $formSubmit = FormSubmit::where('id', $submit_id)
            ->where('form_id', $form_id)
            ->with('user', 'answers')
            ->firstOrFail();

            return response()->json([
                'course' => $course,
                'unit' => $unit,
                'form' => $form,
                'formSubmit' => $formSubmit
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'unit_id'     => 'required',
                'name' => 'required|string',
                'description' => 'required|string',
            ]);

            $user = Auth::user();

            $count = UnitContent::where('unit_contentable_id', '=', $request->unit_id)
            ->count();

            $form = new Form([
                'name'     => $request->name,
                'description'     => $request->description,
                'cta_text'     => $request->cta_text,
            ]);
            $form->save();

            $content = new UnitContent([
                'user_id'    => $user->id,
                'unit_id'    => $request->unit_id,
                'unit_contentable_id'    => $form->id,
                'unit_contentable_type'    => 'App\Models\Form',
                'order'     => $count,
            ]);
            $content->save();

            $content['unit_contentable'] = $form;

            return response()->json([
                'message' => 'The form was successfully created!',
                'content' => $content
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $form->name = $request->name;
            $form->description = $request->description;
            $form->cta_text = $request->cta_text ? $request->cta_text : null;
            $form->save();

            return response()->json([
                'message' => 'The form was successfully updated!',
                'form' => $form,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
            ]);

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();
            $form->unit_content->delete();
            $form->delete();

            return response()->json([
                'message' => 'The form was successfully removed!',
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit_form(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id'     => 'required',
                'form_id' => 'required',
                'form_contents' => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->with('contents')
            ->firstOrFail();

            $form_submit = $form->form_submits()->create([
                'user_id' => $user->id,
                'group_id' => $request->group_id,
                'is_checked' => '0',
            ]);

            $request_form_contents = json_decode($request->form_contents);

            for ($i = 0; $i < count($request_form_contents); $i++) {
                $request_form_content = $request_form_contents[$i];
                $form_content = $form->contents()->where('id', $request_form_content->id)->first();

                $types = [
                    "App\Models\FormContentSelect" => function() use($user, $form_submit, $request, $request_form_content, $form_content) {
                        $hasAnswers = property_exists($request_form_content?->form_contentable?->question, "answers");
                        $request_answers = $hasAnswers ? $request_form_content->form_contentable->question->answers : "";

                        $answer = $form_submit->answers()->create([
                            'user_id' => $user->id,
                            'question_id' => $form_content->form_contentable->question->id,
                            'group_id' => $request->group_id,
                            'content' => $request_answers,
                        ]);

                        $question = Question::with('options', 'options.correct')
                        ->find($form_content->form_contentable->question->id);
                        $is_correct = "0";

                        for ($x=0; $x < count($question->options); $x++) {
                            $option = $question->options[$x];
                            if($option->correct->is_correct){
                                if ($request_answers == $option->content) {
                                    $is_correct = "1";
                                }
                            }
                        }

                        $answer->correct()->create([
                            'user_id' => $user->id,
                            'is_correct' => $is_correct
                        ]);
                    },
                    "App\Models\FormContentMultiple" => function() use($user, $form_submit, $request, $request_form_content, $form_content) {
                        $hasAnswers = property_exists($request_form_content?->form_contentable?->question, "answers");
                        $request_answers = $hasAnswers ? $request_form_content->form_contentable->question->answers : [];

                        if (count($request_answers) > 0) {
                            for ($y=0; $y < count($request_answers); $y++) {
                                $answer = $form_submit->answers()->create([
                                    'user_id' => $user->id,
                                    'question_id' => $form_content->form_contentable->question->id,
                                    'group_id' => $request->group_id,
                                    'content' => $request_answers[$y],
                                ]);

                                $question = Question::with('options', 'options.correct')
                                ->find($form_content->form_contentable->question->id);
                                $is_correct = "0";

                                for ($x=0; $x < count($question->options); $x++) {
                                    $option = $question->options[$x];
                                    if($option->correct->is_correct){
                                        if ($request_answers[$y] == $option->content) {
                                            $is_correct = "1";
                                        }
                                    }
                                }

                                $answer->correct()->create([
                                    'user_id' => $user->id,
                                    'is_correct' => $is_correct
                                ]);
                            }
                        }else{
                            $answer = $form_submit->answers()->create([
                                'user_id' => $user->id,
                                'question_id' => $form_content->form_contentable->question->id,
                                'group_id' => $request->group_id,
                                'content' => '',
                            ]);

                            $answer->correct()->create([
                                'user_id' => $user->id,
                                'is_correct' => '0'
                            ]);
                        }
                    },
                    "App\Models\FormContentQuestion" => function() use($user, $form_submit, $request, $request_form_content, $form_content) {
                        $hasAnswers = property_exists($request_form_content?->form_contentable?->question, "answers");
                        $request_answers = $hasAnswers ? $request_form_content->form_contentable->question->answers : "";

                        $answer = $form_submit->answers()->create([
                            'user_id' => $user->id,
                            'question_id' => $form_content->form_contentable->question->id,
                            'group_id' => $request->group_id,
                            'content' => $request_answers,
                        ]);

                        $answer->correct()->create([
                            'user_id' => $user->id,
                            'is_correct' => '0'
                        ]);
                    },
                    "App\Models\FormContentFillintheblank" => function() use($user, $form_submit, $request, $request_form_content, $form_content) {
                        $request_contents = $request_form_content->form_contentable->contents;

                        for ($y=0; $y < count($request_contents); $y++) {
                            $request_content = $request_contents[$y];

                            if(!Str::contains($request_content->fillintheblank_contentable_type, 'FillintheblankText')){
                                $hasAnswers = property_exists($request_content->fillintheblank_contentable->question, "answers");
                                $request_answers = $hasAnswers ? $request_content->fillintheblank_contentable->question->answers : "";

                                $answer = $form_submit->answers()->create([
                                    'user_id' => $user->id,
                                    'question_id' => $request_content->fillintheblank_contentable->question->id,
                                    'group_id' => $request->group_id,
                                    'content' => $request_answers
                                ]);

                                $question = Question::with('options', 'options.correct')
                                ->find($request_content->fillintheblank_contentable->question->id);
                                $is_correct = "0";

                                for ($x=0; $x < count($question->options); $x++) {
                                    $option = $question->options[$x];
                                    if($option->correct->is_correct){
                                        if ($request_answers == $option->content) {
                                            $is_correct = "1";
                                            if(Str::contains($request_content->fillintheblank_contentable_type, 'FillintheblankInput')){
                                                break;
                                            }
                                        }
                                    }
                                }

                                $answer->correct()->create([
                                    'user_id' => $user->id,
                                    'is_correct' => $is_correct
                                ]);
                            }
                        }
                    },
                    "default" => function(){}
                ];
                (
                    array_key_exists($form_content?->form_contentable_type, $types) ?
                    $types[$form_content?->form_contentable_type]() :
                    $types["default"]()
                );
            }

            return response()->json([
                'message' => 'Form submitted successfully!',
                'form_submit' => $form_submit
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }

    }

    private function getAnwersData($dataAnswers)
    {
        $answers = (object)[
            'total_answers' => 0,
            'data' => (object)[
                'day' => null,
                'month' => null,
                'year' => null
            ]
        ];

        $answers->total_answers = count($dataAnswers);

        // by Day
        $auxReturn = $this->formatDates($dataAnswers, 'Y-m-d');
        $answers->data->day = $auxReturn;

        // by Month
        $auxReturn = $this->formatDates($dataAnswers, 'Y-m');
        $answers->data->month = $auxReturn;

        // by Year
        $auxReturn = $this->formatDates($dataAnswers, 'Y');
        $answers->data->year = $auxReturn;

        return $answers;
    }

    private function formatDates($model, $format)
    {
        $auxReturn = (array)[];

        for ($i=0; $i < count($model); $i++) {
            $dateString = $model[$i]->created_at->format($format);
            array_push($auxReturn, $dateString);
        }
        // filter and count
        $collection = collect($auxReturn);
        $count = $collection->countBy();
        $auxReturn = $count->all();

        return $auxReturn;
    }
}
