<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupNotification;
use App\Models\GroupUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GroupNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $notifications = GroupNotification::where('group_id', '=', $request->group_id)
            ->get();

            return response()->json([
                'notifications' => $notifications,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try {
            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $notification = GroupNotification::where('id', $request->notification_id)
            ->where('group_id', '=', $request->group_id)
            ->firstOrFail();

            return response()->json([
                'notification' => $notification,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function group(Request $request)
    {
        try {
            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->firstOrFail();

            $notification = GroupNotification::where('group_id', '=', $request->group_id)
            ->firstOrFail();

            return response()->json([
                'notification' => $notification,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function store(Request $request){
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'content' => 'required|string',
                'type' => 'required|string',
            ]);

            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $notifications = GroupNotification::where('group_id', '=', $request->group_id)
            ->get();

            foreach ($notifications as $notification) {
                $notification->delete();
            }

            $notification = new GroupNotification([
                'group_id' => $request->group_id,
                'content' => $request->content,
                'type' => $request->type,
            ]);
            $notification->save();

            return response()->json([
                'message' => 'Notification was successfully created!',
                'notification' => $notification,
            ], 200);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'notification_id' => 'required',
                'name' => 'required|string',
                'type' => 'required|string',
            ]);

            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $notification = GroupNotification::where('id', $request->notification_id)
            ->where('group_id', '=', $request->group_id)
            ->firstOrFail();

            $notification->name = $request->name;
            $notification->type = $request->type;
            $notification->save();

            return response()->json([
                'message' => 'The notification was successfully updated!',
                'notification' => $notification,
            ], 200);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'group_id' => 'required',
                'notification_id' => 'required',
            ]);

            $user = Auth::user();

            $role = GroupUser::where('group_id', '=', $request->group_id)
            ->where('user_id', '=', $user->id)
            ->where(function ($query) {
                $query->where('role_user', '=', 'Owner')
                    ->orWhere('role_user', '=', 'Admin');
            })
            ->firstOrFail();

            $notification = GroupNotification::where('id', $request->notification_id)
            ->where('group_id', '=', $request->group_id)
            ->firstOrFail();
            $notification->delete();

            return response()->json([
                'message' => 'The notification was successfully removed!',
                'notification' => $notification,
            ], 200);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }
}
