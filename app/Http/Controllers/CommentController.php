<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_comment_group(Request $request)
    {
        try{
            $this->validate($request, [
                'content' => 'required|string',
                'group_id' => 'required',
            ]);
            $user = Auth::user();

            $comment = new Comment([
                'content'     => $request->content,
                'commentable_id'     => $request->group_id,
                'commentable_type'     => 'App\Models\Group',
                'user_id'    => $user->id,
                'message_id'    => null,
                'course_id'    => null,
                'unit_id'    => null,
                'lesson_id'    => null,
            ]);
            $comment->save();
            $comment['user'] = $user;

            return response()->json([
                'message' => 'The comment was sent successfully!',
                'comment' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete_comment(Request $request)
    {
        try{
            $this->validate($request, [
                'comment_id' => 'required',
            ]);

            $comment = Comment::where('id', $request->comment_id)
            ->firstOrFail();

            $comment->delete();

            return response()->json([
                'message' => 'The comment was successfully removed!',
                'course' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_comment(Request $request)
    {
        try{
            $this->validate($request, [
                'comment_id' => 'required',
                'content' => 'required|string',
            ]);
            $user = Auth::user();

            $comment = Comment::where('id', $request->comment_id)
            ->firstOrFail();

            $comment->content = $request->content;
            $comment->save();

            return response()->json([
                'message' => 'The comment was successfully updated!',
                'comment' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_comment_course(Request $request)
    {
        try{
            $this->validate($request, [
                'content' => 'required|string',
                'group_id' => 'required',
                'course_id' => 'required',
            ]);
            $user = Auth::user();

            $comment = new Comment([
                'content'     => $request->content,
                'commentable_id'     => $request->group_id,
                'commentable_type'     => 'App\Models\Group',
                'user_id'    => $user->id,
                'message_id'    => null,
                'course_id'    => $request->course_id,
                'unit_id'    => null,
                'lesson_id'    => null,
            ]);
            $comment->save();
            $comment['user'] = $user;

            return response()->json([
                'message' => 'The comment was sent successfully!',
                'comment' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_comment_unit(Request $request)
    {
        try{
            $this->validate($request, [
                'content' => 'required|string',
                'group_id' => 'required',
                'unit_id' => 'required',
            ]);
            $user = Auth::user();

            $comment = new Comment([
                'content'     => $request->content,
                'commentable_id'     => $request->group_id,
                'commentable_type'     => 'App\Models\Group',
                'user_id'    => $user->id,
                'message_id'    => null,
                'course_id'    => null,
                'unit_id'    => $request->unit_id,
                'lesson_id'    => null,
            ]);
            $comment->save();
            $comment['user'] = $user;


            return response()->json([
                'message' => 'The comment was sent successfully!',
                'comment' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_comment_lesson(Request $request)
    {
        try{
            $this->validate($request, [
                'content' => 'required|string',
                'group_id' => 'required',
                'lesson_id' => 'required',
            ]);
            $user = Auth::user();

            $comment = new Comment([
                'content'     => $request->content,
                'commentable_id'     => $request->group_id,
                'commentable_type'     => 'App\Models\Group',
                'user_id'    => $user->id,
                'message_id'    => null,
                'course_id'    => null,
                'unit_id'    => null,
                'lesson_id'    => $request->lesson_id,
            ]);
            $comment->save();
            $comment['user'] = $user;

            return response()->json([
                'message' => 'The comment was sent successfully!',
                'comment' => $comment,
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }
}
