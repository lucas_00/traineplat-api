<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\FormContent;
use App\Models\FormContentFillintheblank;
use App\Models\FillintheblankContent;
use App\Models\FillintheblankInput;
use App\Models\FillintheblankSelect;
use App\Models\FillintheblankText;
use App\Models\FormContentQuestion;
use App\Models\FormContentMultiple;
use App\Models\FormContentSelect;
use App\Models\FormContentText;
use App\Models\FormContentVideo;
use App\Models\FormContentLink;
use App\Models\FormContentImage;

use App\Models\File;
use App\Models\Answer;
use App\Models\FormSubmit;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Traits\FileUpload;

use Illuminate\Support\Facades\Log;

class FormContentController extends Controller
{
    use FileUpload;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_fillintheblank_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'form_content_fillintheblanks' => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentFillintheblank = FormContentFillintheblank::create([
                'content' => $request->content,
                'user_id' => $user->id,
            ]);

            $formContent = $formContentFillintheblank->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $formContent['form_contentable'] = $formContentFillintheblank;

            $decoded_form_content_fillintheblanks = json_decode($request->form_content_fillintheblanks);

            for ($i=0; $i < count($decoded_form_content_fillintheblanks); $i++) {
                $content = $decoded_form_content_fillintheblanks[$i];
                $order = $content->order;

                $types = [
                    "FillintheblankText" => function() use($user, $content, $order, $formContent, $formContentFillintheblank) {
                        $fillintheblankText = FillintheblankText::create([
                            'user_id' => $user->id,
                            'content' => $content->fillintheblank_contentable->content
                        ]);
                        $fillintheblankText->save();

                        $fillintheblankContent = FillintheblankContent::create([
                            'user_id' => $user->id,
                            'order' => $order,
                            'form_content_fillintheblank_id' => $formContentFillintheblank->id,
                            'fillintheblank_contentable_id' => $fillintheblankText->id,
                            'fillintheblank_contentable_type' => "App\Models\FillintheblankText"
                        ]);
                        $fillintheblankContent->save();
                    },
                    "FillintheblankInput" => function() use($user, $content, $order, $formContent, $formContentFillintheblank) {
                        $fillintheblankInput = FillintheblankInput::create([
                            'user_id' => $user->id
                        ]);
                        $fillintheblankInput->save();

                        $fillintheblankContent = FillintheblankContent::create([
                            'user_id' => $user->id,
                            'order' => $order,
                            'form_content_fillintheblank_id' => $formContentFillintheblank->id,
                            'fillintheblank_contentable_id' => $fillintheblankInput->id,
                            'fillintheblank_contentable_type' => "App\Models\FillintheblankInput"
                        ]);
                        $fillintheblankContent->save();

                        $question = $fillintheblankInput->question()->create([
                            "user_id" => $user->id,
                            "content" => $content->fillintheblank_contentable->question->content
                        ]);

                        for ($x = 0; $x < count($content->fillintheblank_contentable->options); $x++) {
                            $option = $fillintheblankInput->question->options()->create([
                                "user_id" => $user->id,
                                "content" => $content->fillintheblank_contentable->options[$x]->content
                            ]);

                            $option->correct()->create([
                                'user_id' => $user->id,
                                'is_correct' => $content->fillintheblank_contentable->options[$x]->correct->is_correct
                            ]);

                            $option['correct'] = $option->correct;
                        }
                    },
                    "FillintheblankSelect" => function() use($user, $content, $order, $formContent, $formContentFillintheblank) {
                        $fillintheblankSelect = FillintheblankSelect::create([
                            'user_id' => $user->id,
                        ]);
                        $fillintheblankSelect->save();

                        $fillintheblankContent = FillintheblankContent::create([
                            'user_id' => $user->id,
                            'order' => $order,
                            'form_content_fillintheblank_id' => $formContentFillintheblank->id,
                            'fillintheblank_contentable_id' => $fillintheblankSelect->id,
                            'fillintheblank_contentable_type' => "App\Models\FillintheblankSelect"
                        ]);
                        $fillintheblankContent->save();

                        $question = $fillintheblankSelect->question()->create([
                            "user_id" => $user->id,
                            "content" => $content->fillintheblank_contentable->question->content
                        ]);

                        for ($x = 0; $x < count($content->fillintheblank_contentable->options); $x++) {
                            $option = $fillintheblankSelect->question->options()->create([
                                "user_id" => $user->id,
                                "content" => $content->fillintheblank_contentable->options[$x]->content
                            ]);

                            $option->correct()->create([
                                'user_id' => $user->id,
                                'is_correct' => $content->fillintheblank_contentable->options[$x]->correct->is_correct
                            ]);

                            $option['correct'] = $option->correct;
                        }
                    },
                    "default" => function(){}
                ];
                (
                    array_key_exists($content->fillintheblank_contentable_type, $types) ?
                    $types[$content->fillintheblank_contentable_type]() :
                    $types["default"]()
                );
            }

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_fillintheblank_text_content_update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
                "content" => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankText = $fillintheblankContent->fillintheblank_contentable;

            $fillintheblankText->content = $request->content;
            $fillintheblankText->save();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fillintheblank_question_content_update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
                "content" => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankText = $fillintheblankContent->fillintheblank_contentable;
            $question = $fillintheblankText->question;
            $question->content = $request->content;
            $question->save();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_fillintheblank_option_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'name' => 'required|string',
                'options' => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $fillintheblankOption = FillintheblankOption::create([
                'user_id' => $user->id,
            ]);

            $formContent = $fillintheblankOption->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $question = $fillintheblankOption->question()->create([
                'user_id' => $user->id,
                'content' => $request->name
            ]);

            $formContent['form_contentable'] = $fillintheblankOption;
            $formContent->form_contentable['question'] = $question;

            $options = [];
            $requestOptions = json_decode($request->options);

            for ($i=0; $i < count($requestOptions); $i++) {
                $option = $formContent->form_contentable->question->options()->create([
                    'user_id' => $user->id,
                    'content' => $requestOptions[$i]->content,
                ]);

                $option->correct()->create([
                    'user_id' => $user->id,
                    'is_correct' => $requestOptions[$i]->is_correct
                ]);

                $option['correct'] = $option->correct;

                array_push($options, $option);
            }

            $formContent->form_contentable->question['options'] = $options;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_form_content_fillintheblank_input(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();

            $fillintheblankInput = FillintheblankInput::create([
                'user_id' => $user->id
            ]);
            $fillintheblankInput->save();

            $fillintheblankContent = FillintheblankContent::create([
                'user_id' => $user->id,
                "order" => 0,
                "form_content_fillintheblank_id" => $formContent->form_contentable->id,
                "fillintheblank_contentable_id" => $fillintheblankInput->id,
                "fillintheblank_contentable_type" => "App\Models\FillintheblankInput",
            ]);
            $fillintheblankContent->save();

            $question = $fillintheblankInput->question()->create([
                "user_id" => $user->id,
                "content" => ""
            ]);

            $option = $fillintheblankInput->question->options()->create([
                "user_id" => $user->id,
                "content" => ""
            ]);

            $option->correct()->create([
                'user_id' => $user->id,
                'is_correct' => 1
            ]);

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_form_content_fillintheblank_select(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();

            $fillintheblankSelect = FillintheblankSelect::create([
                'user_id' => $user->id
            ]);
            $fillintheblankSelect->save();

            $fillintheblankContent = FillintheblankContent::create([
                'user_id' => $user->id,
                "order" => 0,
                "form_content_fillintheblank_id" => $formContent->form_contentable->id,
                "fillintheblank_contentable_id" => $fillintheblankSelect->id,
                "fillintheblank_contentable_type" => "App\Models\FillintheblankSelect",
            ]);
            $fillintheblankContent->save();

            $question = $fillintheblankSelect->question()->create([
                "user_id" => $user->id,
                "content" => ""
            ]);

            $option = $fillintheblankSelect->question->options()->create([
                "user_id" => $user->id,
                "content" => ""
            ]);

            $option->correct()->create([
                'user_id' => $user->id,
                'is_correct' => 1
            ]);

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_fillintheblank_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
                "option_id" => 'required',
                "content" => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankElement = $fillintheblankContent->fillintheblank_contentable;
            $question = $fillintheblankElement->question;

            $option = $question->options()->where("id", $request->option_id)->firstOrFail();

            $option->content = $request->content;
            $option->save();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_fillintheblank_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankElement = $fillintheblankContent->fillintheblank_contentable;
            $question = $fillintheblankElement->question;

            $option = $question->options()->create([
                'user_id' => $user->id,
                'content' => "",
            ]);

            $option->correct()->create([
                'user_id' => $user->id,
                'is_correct' => "1"
            ]);

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy_fillintheblank_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
                "option_id" => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankElement = $fillintheblankContent->fillintheblank_contentable;
            $question = $fillintheblankElement->question;

            $option = $question->options()->where("id", $request->option_id)->firstOrFail();
            $option->delete();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_correct_fillintheblank_select(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
                "option_id" => 'required'
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankElement = $fillintheblankContent->fillintheblank_contentable;
            $question = $fillintheblankElement->question;

            $option = $question->options()->where("id", $request->option_id)->firstOrFail();

            if ($option->correct->is_correct == "1") {
                $option->correct->is_correct = 0;
            }else {
                $option->correct->is_correct = 1;
            }

            $option->correct->save();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy_fillintheblank_content(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                "form_content_id" => 'required',
                "form_content_fillintheblank_id" => 'required',
                "fillintheblank_content_id" => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)->firstOrFail();
            $formContent = $form->contents()->where('id', $request->form_content_id)->firstOrFail();
            $formContentFillintheblank = $formContent->form_contentable()->contents()->where('id', $request->fillintheblank_content_id)->firstOrFail();
            $fillintheblankContent = $formContentFillintheblank->contents()->where('id', $request->form_content_fillintheblank_id)->firstOrFail();
            $fillintheblankText = $fillintheblankContent->fillintheblank_contentable;

            $fillintheblankText->delete();
            $fillintheblankContent->delete();

            $auxFormContent = FormContent::where('id', $formContent->id)
            ->select('id', 'order', 'form_contentable_id', 'form_contentable_type', 'form_id')
            ->with([
                'form_contentable:id',
                'form_contentable.contents:id,fillintheblank_contentable_id,fillintheblank_contentable_type,form_content_fillintheblank_id,order',
                'form_contentable.contents.fillintheblank_contentable',
                'form_contentable.contents.fillintheblank_contentable.question:id,content,questionable_id,questionable_type',
                'form_contentable.contents.fillintheblank_contentable.question.options:id,question_id,content',
                'form_contentable.contents.fillintheblank_contentable.question.options.correct:id,correctable_id,correctable_type,is_correct',
            ])
            ->firstOrFail();

            return response()->json([
                'content' => $auxFormContent,
                'message' => 'The content was successfully updated!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_question_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'name' => 'required|string',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentQuestion = FormContentQuestion::create([
                'user_id' => $user->id,
            ]);

            $formContent = $formContentQuestion->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $question = $formContentQuestion->question()->create([
                'user_id' => $user->id,
                'content' => $request->name
            ]);

            $formContent['form_contentable'] = $formContentQuestion;
            $formContent->form_contentable['question'] = $question;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_select_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'name' => 'required|string',
                'options' => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentSelect = FormContentSelect::create([
                'user_id' => $user->id,
            ]);

            $formContent = $formContentSelect->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $question = $formContentSelect->question()->create([
                'user_id' => $user->id,
                'content' => $request->name
            ]);

            $formContent['form_contentable'] = $formContentSelect;
            $formContent->form_contentable['question'] = $question;

            $options = [];
            $requestOptions = json_decode($request->options);

            for ($i=0; $i < count($requestOptions); $i++) {
                $option = $formContent->form_contentable->question->options()->create([
                    'user_id' => $user->id,
                    'content' => $requestOptions[$i]->content,
                ]);

                $option->correct()->create([
                    'user_id' => $user->id,
                    'is_correct' => $requestOptions[$i]->is_correct
                ]);

                $option['correct'] = $option->correct;

                array_push($options, $option);
            }

            $formContent->form_contentable->question['options'] = $options;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_multiple_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'name' => 'required|string',
                'options' => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentMultiple = FormContentMultiple::create([
                'user_id' => $user->id,
            ]);

            $formContent = $formContentMultiple->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $question = $formContentMultiple->question()->create([
                'user_id' => $user->id,
                'content' => $request->name
            ]);

            $formContent['form_contentable'] = $formContentMultiple;
            $formContent->form_contentable['question'] = $question;

            $options = [];
            $requestOptions = json_decode($request->options);

            for ($i=0; $i < count($requestOptions); $i++) {
                $option = $formContent->form_contentable->question->options()->create([
                    'user_id' => $user->id,
                    'content' => $requestOptions[$i]->content,
                ]);

                $option->correct()->create([
                    'user_id' => $user->id,
                    'is_correct' => $requestOptions[$i]->is_correct
                ]);

                $option['correct'] = $option->correct;

                array_push($options, $option);
            }

            $formContent->form_contentable->question['options'] = $options;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_text_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'content' => 'required|string',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentText = FormContentText::create([
                'content' => $request->content
            ]);

            $formContent = $formContentText->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $formContent['form_contentable'] = $formContentText;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_text_update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'form_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $formContent = FormContent::where('id', $request->form_content_id)
            ->firstOrFail();

            $formContent->form_contentable->content = $request->content;
            $formContent->form_contentable->save();

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_link_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'content' => 'required|string',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentLink = FormContentLink::create([
                'content' => $request->content
            ]);

            $formContent = $formContentLink->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $formContent['form_contentable'] = $formContentLink;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_link_update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'form_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $formContent = FormContent::where('id', $request->form_content_id)
            ->firstOrFail();

            $formContent->form_contentable->content = $request->content;
            $formContent->form_contentable->save();

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_video_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'content' => 'required|string',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentVideo = FormContentVideo::create([
                'content' => $request->content
            ]);

            $formContent = $formContentVideo->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $formContent['form_contentable'] = $formContentVideo;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_video_update(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
                'form_content_id' => 'required',
                'content' => 'required|string',
            ]);

            $formContent = FormContent::where('id', $request->form_content_id)
            ->firstOrFail();

            $formContent->form_contentable->content = $request->content;
            $formContent->form_contentable->save();

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_image_store(Request $request)
    {
        try{
            $this->validate($request, [
                'form_id' => 'required',
            ]);

            $user = Auth::user();

            $form = Form::where('id', $request->form_id)
            ->firstOrFail();

            $formContentImage = FormContentImage::create();

            $images = array();

            for ($i = 0; $i < count($request->files); $i++) {
                if ($request->hasFile('files-' . $i)) {
                    $requestFile = $request->file('files-' . $i);
                    $file = $this->saveFiles($requestFile, 'images/');

                    $image = new File([
                        'user_id'    => $user->id,
                        'fileable_id' => $formContentImage->id,
                        'fileable_type' => 'App\Models\FormContentImage',
                        'title'     => $file,
                        'original_name'     => $requestFile->getClientOriginalName(),
                        'src'     => ('/uploads/images/' . $file),
                    ]);

                    $image->save();

                    array_push($images, $image);
                }
            }

            $formContentImage->contents = $images;

            $formContent = $formContentImage->form_content()->create([
                'user_id' => $user->id,
                'form_id' => $form->id,
                'order' => 0,
            ]);

            $formContent['form_contentable'] = $formContentImage;

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function content_image_append(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
            ]);

            $user = Auth::user();

            $formContent = FormContent::where('id', $request->form_content_id)
            ->with('form_contentable')
            ->firstOrFail();

            $images = array();

            for ($i = 0; $i < count($request->files); $i++) {
                if ($request->hasFile('files-' . $i)) {
                    $requestFile = $request->file('files-' . $i);
                    $file = $this->saveFiles($requestFile, 'images/');

                    $image = new File([
                        'user_id'    => $user->id,
                        'fileable_id' => $formContent->form_contentable->id,
                        'fileable_type' => 'App\Models\FormContentImage',
                        'title'     => $file,
                        'original_name'     => $requestFile->getClientOriginalName(),
                        'src'     => ('/uploads/images/' . $file),
                    ]);

                    $image->save();

                    array_push($images, $image);
                }
            }

            $formContent->form_contentable->contents = $images;

            return response()->json([
                'content' => $formContent,
                'images' => $images,
                'message' => 'The content was successfully created!'
            ], 201);

        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        } catch(\Illuminate\Validation\ValidationException $e){

            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function content_image_destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'image_id' => 'required',
            ]);

            $image = File::where('id', $request->image_id)
            ->firstOrFail();
            $image->delete();

            return response()->json([
                'image' => $image,
                'message' => 'The image was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormContent  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
            ]);

            $formContent = FormContent::where('id', $request->form_content_id)
            ->firstOrFail();
            $formContent->delete();

            return response()->json([
                'content' => $formContent,
                'message' => 'The content was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormContent  $content
     * @return \Illuminate\Http\Response
     */
    public function update_question(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
                'question_id' => 'required',
                'question_content' => 'required|string'
            ]);

            $question = Question::where('id', $request->question_id)
            ->firstOrFail();
            $question->content = $request->question_content;
            $question->save();

            return response()->json([
                'question' => $question,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function add_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
            ]);

            $user = Auth::user();

            $formContent = FormContent::where('id', $request->form_content_id)
            ->with('form_contentable')
            ->firstOrFail();

            $option = new Option([
                'user_id' => $user->id,
                'question_id' => $formContent->form_contentable->question->id,
                'content' => '',
            ]);
            $option->save();

            $option->correct()->create([
                'user_id' => $user->id,
                'is_correct' => '0'
            ]);

            $option['correct'] = $option->correct;

            return response()->json([
                'option' => $option,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully created!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function update_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
                'option_id' => 'required',
                'option_content' => 'required|string'
            ]);

            $option = Option::where('id', $request->option_id)
            ->firstOrFail();
            $option->content = $request->option_content;
            $option->save();

            return response()->json([
                'option' => $option,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function update_correct_select(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
                'option_id' => 'required',
            ]);

            $option = Option::where('id', $request->option_id)
            ->firstOrFail();
            $options = $option->question->options;

            for ($i=0; $i < count($options); $i++) {
                if ($options[$i]->id == $request->option_id) {
                    $options[$i]->correct->is_correct = '1';
                    $options[$i]->correct->save();
                }else {
                    $options[$i]->correct->is_correct = '0';
                    $options[$i]->correct->save();
                }
            }

            return response()->json([
                'option' => $option,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function update_correct_multiple(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
                'option_id' => 'required',
            ]);

            $option = Option::where('id', $request->option_id)
            ->firstOrFail();

            if ($option->correct->is_correct == '1') {
                $option->correct->is_correct = '0';
            }else {
                $option->correct->is_correct = '1';
            }

            $option->correct->save();

            return response()->json([
                'option' => $option,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function set_correct_answer(Request $request)
    {
        try{
            $this->validate($request, [
                'answer_id' => 'required',
            ]);

            $answer = Answer::where('id', $request->answer_id)
            ->firstOrFail();

            if ($answer->correct->is_correct == '1') {
                $answer->correct->is_correct = '0';
            }else {
                $answer->correct->is_correct = '1';
            }

            $answer->correct->save();

            return response()->json([
                'answer' => $answer,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function set_checked(Request $request)
    {
        try{
            $this->validate($request, [
                'form_submit_id' => 'required',
            ]);

            $formSubmit = FormSubmit::where('id', $request->form_submit_id)
            ->firstOrFail();

            if ($formSubmit->is_checked == '1') {
                $formSubmit->is_checked = '0';
            }else {
                $formSubmit->is_checked = '1';
            }

            $formSubmit->save();

            return response()->json([
                'formSubmit' => $formSubmit,
                'message' => 'The content was successfully updated!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }

    public function destroy_option(Request $request)
    {
        try{
            $this->validate($request, [
                'form_content_id' => 'required',
                'option_id' => 'required',
            ]);

            $option = Option::where('id', $request->option_id)
            ->firstOrFail();
            $option->delete();

            return response()->json([
                'option' => $option,
                'form_content_id' => $request->form_content_id,
                'message' => 'The content was successfully removed!',
            ], 201);

        } catch(\Illuminate\Validation\ValidationException $e){
            return response([
                'message' => 'Something went wrong on the server'
            ], $e->status);
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => 'No records found'
            ], 404);
        }
    }
}
