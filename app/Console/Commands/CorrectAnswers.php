<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Answer;

class CorrectAnswers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:answers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $answers = Answer::get();
        for ($i=0; $i < count($answers); $i++) {
            if ($answers[$i]->correct == null) {
                $answers[$i]->correct()->create([
                    'user_id' => $answers[$i]->user_id,
                    'is_correct' => '0'
                ]);
            }
        }
        return 0;
    }
}
