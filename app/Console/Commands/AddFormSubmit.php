<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Answer;
use App\Models\FormSubmit;

class AddFormSubmit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'answers:formSubmit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
// 1
        $formSubmit = FormSubmit::create([
            'user_id' => 5,
            'form_id' => 1,
            'is_checked' => '0',
            'group_id' => 4,
        ]);
        $answer = Answer::findOrFail(1);
        $answer['form_submit_id'] = 1;
        $answer->save();
        $answer = Answer::findOrFail(2);
        $answer['form_submit_id'] = 1;
        $answer->save();
        $answer = Answer::findOrFail(3);
        $answer['form_submit_id'] = 1;
        $answer->save();

// 2
        $formSubmit = FormSubmit::create([
            'user_id' => 5,
            'form_id' => 1,
            'is_checked' => '0',
            'group_id' => 4,
        ]);
        $answer = Answer::findOrFail(4);
        $answer['form_submit_id'] = 2;
        $answer->save();
        $answer = Answer::findOrFail(5);
        $answer['form_submit_id'] = 2;
        $answer->save();
        $answer = Answer::findOrFail(6);
        $answer['form_submit_id'] = 2;
        $answer->save();

// 3
        $formSubmit = FormSubmit::create([
            'user_id' => 1,
            'form_id' => 3,
            'is_checked' => '0',
            'group_id' => 5,
        ]);
        $answer = Answer::findOrFail(7);
        $answer['form_submit_id'] = 3;
        $answer->save();
        $answer = Answer::findOrFail(8);
        $answer['form_submit_id'] = 3;
        $answer->save();

// 4
        $formSubmit = FormSubmit::create([
            'user_id' => 1,
            'form_id' => 3,
            'is_checked' => '0',
            'group_id' => 5,
        ]);
        $answer = Answer::findOrFail(9);
        $answer['form_submit_id'] = 4;
        $answer->save();
        $answer = Answer::findOrFail(10);
        $answer['form_submit_id'] = 4;
        $answer->save();


// 5
        $formSubmit = FormSubmit::create([
            'user_id' => 2,
            'form_id' => 2,
            'is_checked' => '0',
            'group_id' => 3,
        ]);
        $answer = Answer::findOrFail(11);
        $answer['form_submit_id'] = 5;
        $answer->save();
        $answer = Answer::findOrFail(12);
        $answer['form_submit_id'] = 5;
        $answer->save();
        $answer = Answer::findOrFail(13);
        $answer['form_submit_id'] = 5;
        $answer->save();

        return 0;
    }
}
