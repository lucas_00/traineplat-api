<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Option;

class CorrectOptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:options';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $options = Option::get();
        for ($i=0; $i < count($options); $i++) {
            if ($options[$i]->correct == null) {
                $options[$i]->correct()->create([
                    'user_id' => 2,
                    'is_correct' => '0'
                ]);
            }
        }
        return 0;
    }
}
