<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContent extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'lesson_id',
        'lesson_contentable_id',
        'lesson_contentable_type',
        'order',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    /**
     * Get the child lesson_contentable model (LessonContentText, LessonContentVideo, LessonContentImage or LessonContentLink).
     */
    public function lesson_contentable()
    {
        return $this->morphTo();
    }
}
