<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormContentImage extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    // protected $with = array('contents');

    public function contents()
    {
        return $this->hasMany(File::class, 'fileable_id', 'id');
    }

    public function question()
    {
        return $this->morphOne(Question::class, 'questionable');
    }

    public function form_content()
    {
        return $this->morphOne(FormContent::class, 'form_contentable');
    }
}
