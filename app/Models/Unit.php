<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'cta_text'
    ];

    public function contents()
    {
        return $this->hasMany(UnitContent::class);
    }

    public function course_content()
    {
        return $this->morphOne(CourseContent::class, 'course_contentable');
    }
}
