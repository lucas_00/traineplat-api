<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContentImage extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $with = array('contents');

    public function contents()
    {
        return $this->hasMany(File::class, 'fileable_id', 'id');
    }

    public function lesson_content()
    {
        return $this->morphOne(LessonContent::class, 'lesson_contentable');
    }
}
