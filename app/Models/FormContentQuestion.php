<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormContentQuestion extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $with = array('question');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->morphOne(Question::class, 'questionable');
    }

    public function form_content()
    {
        return $this->morphOne(FormContent::class, 'form_contentable');
    }
}
