<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'question_id',
        'form_submit_id',
        'group_id',
        'content',
        'created_at'
    ];

    protected $with = array('user', 'correct');

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function form_submit()
    {
        return $this->belongsTo(FormSubmit::class);
    }

    public function correct()
    {
        return $this->morphOne('App\Models\Correct', 'correctable');
    }
}
