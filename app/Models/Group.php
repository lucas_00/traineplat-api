<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image_id',
        'user_id',
    ];

    protected $with = array('logo');

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function notification()
    {
        return $this->hasOne('App\Models\GroupNotification');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('role_user');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course');
    }

    public function logo()
    {
        return $this->morphOne('App\Models\File', 'fileable');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function form_submits()
    {
        return $this->hasMany(FormSubmit::class);
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
}
