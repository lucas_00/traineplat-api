<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'image_id',
        'user_id',
        'cta_text'
    ];

    protected $with = array('logo');

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('role_user');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\CourseContent');
    }

    public function message()
    {
        return $this->belongsTo('App\Models\Message');
    }

    public function logo()
    {
        return $this->morphOne('App\Models\File', 'fileable');
    }
}
