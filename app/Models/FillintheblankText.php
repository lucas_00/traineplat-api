<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FillintheblankText extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $with = array('question');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->morphOne(Question::class, 'questionable');
    }

    public function fillintheblank_content()
    {
        return $this->morphOne(FillintheblankContent::class, 'fillintheblank_contentable');
    }
}
