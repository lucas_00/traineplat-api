<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormContentText extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
    ];

    public function question()
    {
        return $this->morphOne(Question::class, 'questionable');
    }

    public function form_content()
    {
        return $this->morphOne(FormContent::class, 'form_contentable');
    }
}
