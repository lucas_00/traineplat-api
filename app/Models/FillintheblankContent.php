<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FillintheblankContent extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $with = array('fillintheblank_contentable');
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'form_content_fillintheblank_id',
        'fillintheblank_contentable_id',
        'fillintheblank_contentable_type',
        'order',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function form_content_fillintheblank()
    {
        return $this->belongsTo(FormContentFillintheblank::class);
    }

    /**
     * Get the child fillintheblank_contentable model (FillintheblankText, FillintheblankSelect, FillintheblankInput).
     */
    public function fillintheblank_contentable()
    {
        return $this->morphTo();
    }
}
