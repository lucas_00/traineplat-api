<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormContent extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'form_id',
        'order',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * Get the child form_contentable model (FormContentFillintheblank, FormContentQuestion, FormContentSelect, FormContentMultiple, FormContentText, FormContentVideo, FormContentImage or FormContentLink).
     */
    public function form_contentable()
    {
        return $this->morphTo();
    }
}
