<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Correct extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'is_correct',
        'correctable_id',
        'correctable_type',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the child correctable model (Answer, Option).
     */
    public function correctable()
    {
        return $this->morphTo();
    }
}
