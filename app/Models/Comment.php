<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $with = array('user');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'content',
        'commentable_id',
        'commentable_type',
        'course_id',
        'message_id',
        'unit_id',
        'lesson_id',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the child commentable model (Group).
     */
    public function commentable()
    {
        return $this->morphTo();
    }
}
