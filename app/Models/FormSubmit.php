<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use App\Models\Answer;

class FormSubmit extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $appends = ['date', 'score'];
    // protected $appends = ['date', 'correct_answers', 'questions_count'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'form_id',
        'group_id',
        'is_checked',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function getDateAttribute(){
        return $this->created_at->toDateTimeString();
    }

    public function getScoreAttribute(){
        $score = 0;
        if ($this->correctAnswers > 0 && $this->questionsCount > 0) {
            $score = ($this->correctAnswers / $this->questionsCount) * 100;
        }
        return round($score, 1);
    }

    public function getCorrectAnswersAttribute(){
        $count = 0;
        if ($this->form) {
            if ($this->form->contents) {
                for ($x = 0; $x < count($this->form->contents); $x++) {
                    $content = $this->form->contents[$x];
                    if ($content != null) {
                        $form_contentable = $content->form_contentable;
                        if ($form_contentable != null) {
                            $question = $form_contentable->question;
                            $answers = Answer::select('id', 'content', 'question_id')
                            ->where('form_submit_id', $this->id)
                            ->get();
                            $types = [
                                "App\Models\FormContentSelect" => function() use(&$count, $question, $answers) {
                                    for ($i=0; $i < count($answers); $i++) {
                                        $answer = $answers[$i];
                                        if ($answer->question_id === $question->id) {
                                            if ($answer->correct?->is_correct === "1") {
                                                $count++;
                                                break;
                                            }
                                        }
                                    }
                                },
                                "App\Models\FormContentMultiple" => function() use(&$count, $question, $answers) {
                                    if ($question != null) {
                                        $flag = false;
                                        for ($x = 0; $x < count($question->options); $x++) {
                                            $option = $question->options[$x];
                                            if ($option->correct?->is_correct === "1") {
                                                $flag = false;
                                                for ($i=0; $i < count($answers); $i++) {
                                                    $answer = $answers[$i];
                                                    if ($answer->question_id === $question->id) {
                                                        if ($answer->content === $option->content) {
                                                            $flag = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (!$flag) {
                                                    break;
                                                }
                                            }
                                        }
                                        if ($flag) {
                                            $count++;
                                        }
                                    }
                                },
                                "App\Models\FormContentQuestion" => function() use(&$count, $question, $answers) {
                                    for ($i=0; $i < count($answers); $i++) {
                                        $answer = $answers[$i];
                                        if ($answer->question_id === $question->id) {
                                            if ($answer->correct?->is_correct === "1") {
                                                $count++;
                                                break;
                                            }
                                        }
                                    }
                                },
                                "App\Models\FormContentFillintheblank" => function() use(&$count, $form_contentable, $answers) {
                                    $countCorrectAnswers = 0;
                                    $types = [
                                        "App\Models\FillintheblankInput" => function($fillintheblank_contentable) use(&$count, &$countCorrectAnswers, $answers) {
                                            $question = $fillintheblank_contentable->question;
                                            for ($i=0; $i < count($answers); $i++) {
                                                $answer = $answers[$i];
                                                if ($answer->question_id === $question->id) {
                                                    if ($answer->correct?->is_correct === "1") {
                                                        $countCorrectAnswers++;
                                                        break;
                                                    }
                                                }
                                            }
                                        },
                                        "App\Models\FillintheblankSelect" => function($fillintheblank_contentable) use(&$count, &$countCorrectAnswers, $answers) {
                                            $question = $fillintheblank_contentable->question;
                                            for ($i=0; $i < count($answers); $i++) {
                                                $answer = $answers[$i];
                                                if ($answer->question_id === $question->id) {
                                                    if ($answer->correct?->is_correct === "1") {
                                                        $countCorrectAnswers++;
                                                        break;
                                                    }
                                                }
                                            }
                                        },
                                        "App\Models\FillintheblankText" => function($fillintheblank_contentable) use(&$count, &$countCorrectAnswers, $answers) {
                                            $countCorrectAnswers++;
                                        },
                                        "default" => function(){}
                                    ];
                                    for ($x = 0; $x < count($form_contentable->contents); $x++) {
                                        $fillintheblank_content = $form_contentable->contents[$x];
                                        (
                                            array_key_exists($fillintheblank_content->fillintheblank_contentable_type, $types) ?
                                            $types[$fillintheblank_content->fillintheblank_contentable_type]($fillintheblank_content->fillintheblank_contentable) :
                                            $types["default"]()
                                        );
                                    }
                                    if (count($form_contentable->contents) == $countCorrectAnswers) {
                                        $count++;
                                    }
                                },
                                "default" => function(){}
                            ];
                            (
                                array_key_exists($content->form_contentable_type, $types) ?
                                $types[$content->form_contentable_type]() :
                                $types["default"]()
                            );
                        }
                    }
                }
            }
        }
        return $count;
    }

    public function getQuestionsCountAttribute(){
        $count = 0;
        for ($x=0; $x < count($this->form->contents); $x++) {
            $content = $this->form->contents[$x];
            if ($content != null) {
                $types = [
                    "App\Models\FormContentSelect" => function() use(&$count) {
                        $count++;
                    },
                    "App\Models\FormContentMultiple" => function() use(&$count) {
                        $count++;
                    },
                    "App\Models\FormContentQuestion" => function() use(&$count) {
                        $count++;
                    },
                    "App\Models\FormContentFillintheblank" => function() use(&$count) {
                        $count++;
                    },
                    "default" => function(){}
                ];
                (
                    array_key_exists($content->form_contentable_type, $types) ?
                    $types[$content->form_contentable_type]() :
                    $types["default"]()
                );
            }
        }
        return $count;
    }
}
