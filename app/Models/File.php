<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'fileable_id',
        'fileable_type',
        'title',
        'original_name',
        'src',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the child fileable model (LessonContentImage, Group, Course, User).
     */
    public function fileable()
    {
        return $this->morphTo();
    }
}
