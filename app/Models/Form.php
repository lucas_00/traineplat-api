<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'cta_text'
    ];

    public function contents()
    {
        return $this->hasMany(FormContent::class);
    }

    public function form_submits()
    {
        return $this->hasMany(FormSubmit::class);
    }

    public function unit_content()
    {
        return $this->morphOne(UnitContent::class, 'unit_contentable');
    }
}
