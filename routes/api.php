<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LayoutController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BreadcrumbController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\GroupNotificationController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseContentController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\LessonContentController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\FormContentController;
use App\Http\Controllers\SubmitController;
use App\Http\Controllers\UserProgressController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    $response = [
        'user' => $request->user()
    ];

    return response($response, 201);
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::put('/user/name', [UserController::class, 'update_name'])->name('user.update_name');
    Route::put('/user/email', [UserController::class, 'update_email'])->name('user.update_email');
    Route::put('/user/password', [UserController::class, 'update_password'])->name('user.update_password');

    Route::get('/breadcrumb/get_group_course_unit_form', [BreadcrumbController::class, 'get_group_course_unit_form'])->name('group.get_group_course_unit_form');
    Route::get('/breadcrumb/get_group', [BreadcrumbController::class, 'get_group'])->name('group.get_group');

    Route::get('/layout/index', [LayoutController::class, 'index'])->name('layout.index');

    Route::get('/group/index', [GroupController::class, 'index'])->name('group.index');
    Route::get('/group/dashboard', [GroupController::class, 'dashboard'])->name('group.dashboard');
    Route::get('/group/show/', [GroupController::class, 'show'])->name('group.show');
    Route::post('/group/store', [GroupController::class, 'store'])->name('group.store');
    Route::put('/group/update', [GroupController::class, 'update'])->name('group.update');
    Route::delete('/group/destroy', [GroupController::class, 'destroy'])->name('group.destroy');

    Route::get('/group/subscriptions', [GroupController::class, 'subscriptions'])->name('group.subscriptions');
    Route::get('/group/course', [GroupController::class, 'course'])->name('group.course');
    Route::get('/group/search_courses', [GroupController::class, 'search_courses'])->name('group.search_courses');
    Route::post('/group/add_course', [GroupController::class, 'add_course'])->name('group.add_course');
    Route::delete('/group/remove_course', [GroupController::class, 'remove_course'])->name('group.remove_course');
    Route::get('/group/unit', [GroupController::class, 'unit'])->name('group.unit');
    Route::get('/group/lesson', [GroupController::class, 'lesson'])->name('group.lesson');
    Route::get('/group/form', [GroupController::class, 'form'])->name('group.form');
    Route::get('/group/show_submit', [GroupController::class, 'show_submit'])->name('group.show_submit');

    Route::get('/group/users', [GroupController::class, 'get_users'])->name('group.get_users');
    Route::post('/group/add_user', [GroupController::class, 'add_user'])->name('group.add_user');
    Route::put('/group/edit_role_user', [GroupController::class, 'edit_role_user'])->name('group.edit_role_user');
    Route::delete('/group/remove_user', [GroupController::class, 'remove_user'])->name('group.remove_user');

    Route::post('/comment/add_comment_group', [CommentController::class, 'add_comment_group'])->name('comment.add_comment_group');
    Route::delete('/comment/delete_comment', [CommentController::class, 'delete_comment'])->name('comment.delete_comment');
    Route::put('/comment/update_comment', [CommentController::class, 'update_comment'])->name('comment.update_comment');
    Route::post('/comment/add_comment_course', [CommentController::class, 'add_comment_course'])->name('comment.add_comment_course');
    Route::post('/comment/add_comment_unit', [CommentController::class, 'add_comment_unit'])->name('comment.add_comment_unit');
    Route::post('/comment/add_comment_lesson', [CommentController::class, 'add_comment_lesson'])->name('comment.add_comment_lesson');

    Route::get('/notification/index/', [GroupNotificationController::class, 'index'])->name('notification.index');
    Route::get('/notification/show/', [GroupNotificationController::class, 'show'])->name('notification.show');
    Route::get('/notification/group/', [GroupNotificationController::class, 'group'])->name('notification.group');
    Route::post('/notification/store/', [GroupNotificationController::class, 'store'])->name('notification.store');
    Route::put('/notification/update/', [GroupNotificationController::class, 'update'])->name('notification.update');
    Route::delete('/notification/destroy', [GroupNotificationController::class, 'destroy'])->name('notification.destroy');

    Route::get('/course/index', [CourseController::class, 'index'])->name('course.index');
    Route::get('/course/dashboard', [CourseController::class, 'dashboard'])->name('course.dashboard');
    Route::get('/course/show/', [CourseController::class, 'show'])->name('course.show');
    Route::post('/course/store', [CourseController::class, 'store'])->name('course.store');
    Route::put('/course/update', [CourseController::class, 'update'])->name('course.update');
    Route::delete('/course/destroy', [CourseController::class, 'destroy'])->name('course.destroy');

    Route::get('/course/users', [CourseController::class, 'get_users'])->name('course.get_users');
    Route::post('/course/add_user', [CourseController::class, 'add_user'])->name('course.add_user');
    Route::put('/course/edit_role_user', [CourseController::class, 'edit_role_user'])->name('course.edit_role_user');
    Route::delete('/course/remove_user', [CourseController::class, 'remove_user'])->name('course.remove_user');

    Route::get('/course_content/index/', [CourseContentController::class, 'index'])->name('course_content.index');

    Route::get('/unit/show/', [UnitController::class, 'show'])->name('unit.show');
    Route::post('/unit/store/', [UnitController::class, 'store'])->name('unit.store');
    Route::put('/unit/update/', [UnitController::class, 'update'])->name('unit.update');
    Route::delete('/unit/destroy', [UnitController::class, 'destroy'])->name('unit.destroy');
    Route::get('/unit/search_forms', [UnitController::class, 'search_forms'])->name('group.search_forms');

    Route::get('/lesson/show/', [LessonController::class, 'show'])->name('lesson.show');
    Route::post('/lesson/store/', [LessonController::class, 'store'])->name('lesson.store');
    Route::put('/lesson/update/', [LessonController::class, 'update'])->name('lesson.update');
    Route::delete('/lesson/destroy/', [LessonController::class, 'destroy'])->name('lesson.destroy');

    Route::post('/lesson_content/store/text/', [LessonContentController::class, 'store_text'])->name('lesson_content.store_text');
    Route::put('/lesson_content/update/text/', [LessonContentController::class, 'update_text'])->name('lesson_content.update_text');

    Route::post('/lesson_content/store/video/', [LessonContentController::class, 'store_video'])->name('lesson_content.store_video');
    Route::put('/lesson_content/update/video/', [LessonContentController::class, 'update_video'])->name('lesson_content.update_video');

    Route::post('/lesson_content/store/link/', [LessonContentController::class, 'store_link'])->name('lesson_content.store_link');
    Route::put('/lesson_content/update/link/', [LessonContentController::class, 'update_link'])->name('lesson_content.update_link');

    Route::post('/lesson_content/store/image/', [LessonContentController::class, 'store_image'])->name('lesson_content.store_image');
    Route::post('/lesson_content/store/append_image/', [LessonContentController::class, 'append_image'])->name('lesson_content.append_image');
    Route::post('/lesson_content/update/image/', [LessonContentController::class, 'update_image'])->name('lesson_content.update_image');
    Route::delete('/lesson_content/destroy/image/', [LessonContentController::class, 'destroy_image'])->name('lesson_content.destroy_image');
    Route::delete('/lesson_content/destroy/', [LessonContentController::class, 'destroy'])->name('lesson_content.destroy');

    Route::get('/form/index', [FormController::class, 'index'])->name('form.index');
    Route::get('/form/show', [FormController::class, 'show'])->name('form.show');
    Route::get('/form/statistics', [FormController::class, 'statistics'])->name('form.statistics');
    Route::get('/form/submit_answers', [FormController::class, 'submit_answers'])->name('form.submit_answers');
    Route::get('/form/show_submit', [FormController::class, 'show_submit'])->name('form.show_submit');
    Route::post('/form/store', [FormController::class, 'store'])->name('form.store');
    Route::put('/form/update', [FormController::class, 'update'])->name('form.update');
    Route::delete('/form/destroy/', [FormController::class, 'destroy'])->name('form.destroy');
    Route::post('/form/submit_form', [FormController::class, 'submit_form'])->name('form.submit_form');

    Route::post('/form_content/content_fillintheblank_store', [FormContentController::class, 'content_fillintheblank_store'])->name('form_content.content_fillintheblank_store');
    Route::post('/form_content/content_fillintheblank_option_store', [FormContentController::class, 'content_fillintheblank_option_store'])->name('form_content.content_fillintheblank_option_store');
    Route::post('/form_content/content_question_store', [FormContentController::class, 'content_question_store'])->name('form_content.content_question_store');
    Route::post('/form_content/content_select_store', [FormContentController::class, 'content_select_store'])->name('form_content.content_select_store');
    Route::post('/form_content/content_multiple_store', [FormContentController::class, 'content_multiple_store'])->name('form_content.content_multiple_store');

    Route::put('/form_content/content_fillintheblank_text_content_update', [FormContentController::class, 'content_fillintheblank_text_content_update'])->name('form_content.content_fillintheblank_text_content_update');
    Route::put('/form_content/fillintheblank_question_content_update', [FormContentController::class, 'fillintheblank_question_content_update'])->name('form_content.fillintheblank_question_content_update');
    Route::put('/form_content/update_fillintheblank_option', [FormContentController::class, 'update_fillintheblank_option'])->name('form_content.update_fillintheblank_option');
    Route::post('/form_content/add_fillintheblank_option', [FormContentController::class, 'add_fillintheblank_option'])->name('form_content.add_fillintheblank_option');
    Route::delete('/form_content/destroy_fillintheblank_option', [FormContentController::class, 'destroy_fillintheblank_option'])->name('form_content.destroy_fillintheblank_option');
    Route::put('/form_content/update_correct_fillintheblank_select', [FormContentController::class, 'update_correct_fillintheblank_select'])->name('form_content.update_correct_fillintheblank_select');
    Route::post('/form_content/add_form_content_fillintheblank_input', [FormContentController::class, 'add_form_content_fillintheblank_input'])->name('form_content.add_form_content_fillintheblank_input');
    Route::post('/form_content/add_form_content_fillintheblank_select', [FormContentController::class, 'add_form_content_fillintheblank_select'])->name('form_content.add_form_content_fillintheblank_select');
    Route::delete('/form_content/destroy_fillintheblank_content', [FormContentController::class, 'destroy_fillintheblank_content'])->name('form_content.destroy_fillintheblank_content');

    Route::post('/form_content/content_text_store', [FormContentController::class, 'content_text_store'])->name('form_content.content_text_store');
    Route::put('/form_content/content_text_update', [FormContentController::class, 'content_text_update'])->name('form_content.content_text_update');
    Route::post('/form_content/content_link_store', [FormContentController::class, 'content_link_store'])->name('form_content.content_link_store');
    Route::put('/form_content/content_link_update', [FormContentController::class, 'content_link_update'])->name('form_content.content_link_update');
    Route::post('/form_content/content_video_store', [FormContentController::class, 'content_video_store'])->name('form_content.content_video_store');
    Route::put('/form_content/content_video_update', [FormContentController::class, 'content_video_update'])->name('form_content.content_video_update');
    Route::post('/form_content/content_image_store', [FormContentController::class, 'content_image_store'])->name('form_content.content_image_store');
    Route::put('/form_content/content_image_append', [FormContentController::class, 'content_image_append'])->name('form_content.content_image_append');
    Route::delete('/form_content/content_image_destroy', [FormContentController::class, 'content_image_destroy'])->name('form_content.content_image_destroy');

    Route::delete('/form_content/destroy', [FormContentController::class, 'destroy'])->name('form_content.destroy');

    Route::post('/form_content/add_option', [FormContentController::class, 'add_option'])->name('form_content.add_option');
    Route::put('/form_content/update_question', [FormContentController::class, 'update_question'])->name('form_content.update_question');
    Route::put('/form_content/update_option', [FormContentController::class, 'update_option'])->name('form_content.update_option');
    Route::put('/form_content/update_correct_select', [FormContentController::class, 'update_correct_select'])->name('form_content.update_correct_select');
    Route::put('/form_content/update_correct_multiple', [FormContentController::class, 'update_correct_multiple'])->name('form_content.update_correct_multiple');
    Route::put('/form_content/set_correct_answer', [FormContentController::class, 'set_correct_answer'])->name('form_content.set_correct_answer');
    Route::put('/form_content/set_checked', [FormContentController::class, 'set_checked'])->name('form_content.set_checked');
    Route::delete('/form_content/destroy_option', [FormContentController::class, 'destroy_option'])->name('form_content.destroy_option');

    Route::get('/submit/get_user_form_submits', [SubmitController::class, 'get_user_form_submits'])->name('group.get_user_form_submits');
    Route::get('/submit/get_group_form_submits', [SubmitController::class, 'get_group_form_submits'])->name('group.get_group_form_submits');

    Route::get('/user_progress/index', [UserProgressController::class, 'index'])->name('user_progress.index');
    Route::get('/user_progress/get_user_form_submits', [UserProgressController::class, 'get_user_form_submits'])->name('user_progress.get_user_form_submits');
});

Route::post('/login', [AuthController::class, 'login'])->name('login');

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::post('/register', [AuthController::class, 'signup'])->name('signup');


Route::group(['middleware' => 'guest'], function () {
    Route::post('/forgot-password', [AuthController::class, 'forgot_password'])->name('password.email');
    Route::post('/reset-password', [AuthController::class, 'reset_password_update'])->name('password.update');
});