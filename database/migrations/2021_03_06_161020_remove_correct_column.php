<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveCorrectColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('answers', 'correct')){
            Schema::table('answers', function (Blueprint $table) {
                $table->dropColumn('correct');
            });
        }

        if (Schema::hasColumn('options', 'correct')){
            Schema::table('options', function (Blueprint $table) {
                $table->dropColumn('correct');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function($table) {
           $table->string('correct');
        });

        Schema::table('options', function($table) {
            $table->string('correct');
         });

    }
}
